package net.rizon;

import static org.junit.Assert.*;

import org.junit.Test;

public class BlowfishCryptTest {
	private BlowfishCrypt crypt = BlowfishCrypt.getInstance();

	@Test
	public final void testEncryptDecrypt() throws Exception {
		byte[] crypted = crypt.encrypt("sfdsfsdfsdfsdf", "thisisakey");
		byte[] decrypted = crypt.decrypt(crypted, "thisisakey".getBytes("UTF-8"));
		assertArrayEquals("sfdsfsdfsdfsdf".getBytes("UTF-8"), decrypted);
	}

	@Test
	public final void testEncryptDecryptBase64() throws Exception {
		assertEquals("dYATXCKri8B5tUaaESgsiGowhbkjW45e", crypt.encryptToBase64("aaaaaaaaaaaaaaaaaaaaa", "thisisakey"));
		assertFalse("dYATXCKri8B5tUaaESgsiGowhbkjW45e".equals(crypt.encryptToBase64("aaaaaaaaaaaaaaaaaaaaa", "thisisakeyy")));
		
		String crypted = crypt.encryptToBase64("xukuyuxfdufyu", "thisisakey");
		String decrypted = crypt.decryptFromBase64AsString(crypted, "thisisakey");
		assertEquals("xukuyuxfdufyu", decrypted);
	}

	@Test
	public final void testEncryptDecryptUTF8() throws Exception {
		String clear = "私は長門有希です。";
		String key = "カノン";
		String crypted = crypt.encryptToBase64(clear, key);
		String decrypted = crypt.decryptFromBase64AsString(crypted, key);
		assertEquals(clear, decrypted);
	}

}
