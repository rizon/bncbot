package net.rizon;

import static org.junit.Assert.*;

import org.junit.Test;

public class Base64Test {

	@Test
	public final void testEncode() {
		assertEquals("c3VyZS4=", Base64.encode(new byte[] { 's', 'u', 'r', 'e', '.' }));
		assertFalse(Base64.encode(new byte[] { 's', 'u', 'r', 'e', '.' }).equals("c3VyZS4=="));
		
		assertEquals(Base64.encodeFromString("sure."), "c3VyZS4=");
		assertFalse(Base64.encodeFromString("sure.").equals("c3VyZS4=="));
	}

	@Test
	public final void testDecode() {
		assertArrayEquals(new byte[] { 's', 'u', 'r', 'e', '.' }, Base64.decode("c3VyZS4="));
		
		assertEquals("sure", Base64.decodeToString("c3VyZQ=="));
	}

}
