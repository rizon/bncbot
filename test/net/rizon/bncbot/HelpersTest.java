package net.rizon.bncbot;

import static org.junit.Assert.*;

import org.junit.Test;

public class HelpersTest {

	@Test
	public final void testIsValidUsername() {
		assertTrue(Helpers.isValidUsername("testing"));
		assertFalse(Helpers.isValidUsername("3testing"));
		assertFalse(Helpers.isValidUsername("testing[lol]"));
		assertTrue(Helpers.isValidUsername("t3sting_lol"));
	}

	@Test
	public final void testMatchesWithWildcards() {
		assertTrue(Helpers.matchesWithWildcards("lolcakes", "lol*"));
		assertTrue(Helpers.matchesWithWildcards("lolcakes", "*lol*"));
		assertTrue(Helpers.matchesWithWildcards("lolcakes", "*olcak*"));
		assertTrue(Helpers.matchesWithWildcards("lolcakes", "*cakes*"));
		assertTrue(Helpers.matchesWithWildcards("lolcakes", "*cakes"));
		assertFalse(Helpers.matchesWithWildcards("lolcakes", "cake*"));
		assertFalse(Helpers.matchesWithWildcards("lolcakes", "*cake"));
		assertFalse(Helpers.matchesWithWildcards("lolcakes", "lol"));
		
		assertTrue(Helpers.matchesWithWildcards("this!is@a.hostmask", "*!*@a.hostmask"));
		assertFalse(Helpers.matchesWithWildcards("this!is@not.a.hostmask", "*!*@a.hostmask"));
	}

}
