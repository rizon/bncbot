-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (i386)
--
-- Host: localhost    Database: rizonbnc
-- ------------------------------------------------------
-- Server version       5.1.73-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Temporary table structure for view `bnc_lastseen`
--

DROP TABLE IF EXISTS `bnc_lastseen`;
/*!50001 DROP VIEW IF EXISTS `bnc_lastseen`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `bnc_lastseen` (
  `user_id` tinyint NOT NULL,
  `event_time` tinyint NOT NULL,
  `event_type` tinyint NOT NULL,
  `ip` tinyint NOT NULL,
  `was_last_client` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `bnc_user_last_conn_event_time`
--

DROP TABLE IF EXISTS `bnc_user_last_conn_event_time`;
/*!50001 DROP VIEW IF EXISTS `bnc_user_last_conn_event_time`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `bnc_user_last_conn_event_time` (
  `user_id` tinyint NOT NULL,
  `event_time` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `bncbot_bl`
--

DROP TABLE IF EXISTS `bncbot_bl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bncbot_bl` (
  `bl_id` int(7) unsigned NOT NULL AUTO_INCREMENT COMMENT 'The ID of the blacklist entry.',
  `bl_data` varchar(255) NOT NULL COMMENT 'The usermask that is blacklisted by this entry.',
  PRIMARY KEY (`bl_id`),
  UNIQUE KEY `bl_data` (`bl_data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Contains the blacklisted usermasks.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bncbot_bncs`
--

DROP TABLE IF EXISTS `bncbot_bncs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bncbot_bncs` (
  `server_id` int(9) unsigned NOT NULL AUTO_INCREMENT COMMENT 'The ID of the BNC server.',
  `server_address` varchar(255) NOT NULL COMMENT 'The address(hostname) of the server.',
  `server_port` int(5) unsigned NOT NULL COMMENT 'The port to connect to.',
  `server_ssl` bit(1) NOT NULL DEFAULT b'0' COMMENT 'Whether or not to use SSL.',
  `server_nick` varchar(64) NOT NULL COMMENT 'The nick to use when connection to the server.',
  `server_ident` varchar(13) NOT NULL COMMENT 'The ident to use when connecting.',
  `server_gecos` varchar(64) NOT NULL COMMENT 'The GECOS field to specify when connecting.',
  `server_pass` varchar(64) CHARACTER SET latin1 NOT NULL COMMENT 'The password to use when connecting to the server.',
  `server_abbr` char(3) NOT NULL COMMENT 'The 3-character abbreviation that represents this server location.',
  `server_user_server` varchar(255) NOT NULL COMMENT 'The server the BNC is instructed to connect to.',
  PRIMARY KEY (`server_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Contains a list of BNC server the bot manages.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bncbot_conn_log`
--

DROP TABLE IF EXISTS `bncbot_conn_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bncbot_conn_log` (
  `user_id` int(10) unsigned NOT NULL COMMENT 'The user who generated the connection event.',
  `event_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The time the connection event was generated.',
  `event_type` enum('attach','detach') CHARACTER SET ascii NOT NULL,
  `ip` int(10) unsigned NOT NULL COMMENT 'The IP (in long format) that generated the connection event.',
  `was_last_client` bit(1) NOT NULL DEFAULT b'0' COMMENT 'Whether or not this detach was the user''s last client. This field has no bearing on attach events.',
  KEY `user_id` (`user_id`),
  KEY `ip` (`ip`),
  KEY `user_id_time` (`user_id`,`event_time`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Stores attach/detach events to the BNC.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bncbot_help`
--

DROP TABLE IF EXISTS `bncbot_help`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bncbot_help` (
  `help_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'The ID of the help entry.',
  `help_command` varchar(20) NOT NULL COMMENT 'The command name.',
  `help_params` varchar(100) NOT NULL COMMENT 'The command parameters.',
  `help_description` text NOT NULL COMMENT 'The command description',
  `help_category` varchar(50) NOT NULL COMMENT 'The command category.',
  PRIMARY KEY (`help_id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COMMENT='Contains help entries for basic in-IRC help.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bncbot_help`
--

LOCK TABLES `bncbot_help` WRITE;
/*!40000 ALTER TABLE `bncbot_help` DISABLE KEYS */;
INSERT INTO `bncbot_help` VALUES (1,'.bncapprove','<id or nick> [<id or nick> <id or nick>...]','Approves the BNC request. Takes variable amount of args.','Request Management'),(2,'.bncreject','<id or nick> <reason>','Rejects the BNC request. Reason can be >1 word and does not require quotes.','Request Management'),(3,'.bncdelete','<id or nick> <reason>','Deletes the BNC account. Request is not deleted but marked as rejected and account on BNC removed.','Request Management'),(4,'.bncsuspend','<id or nick> <expiry> <reason>','Suspends the BNC account. Prevents user from logging in and disconnects BNC from IRC. Account settings preserved. Expiry in minutes.','Request Management'),(5,'.bncunsuspend','<id or nick>','Unsuspends the BNC account. Only works if already suspended.','Request Management'),(6,'.bncinfo','<id or nick>','Gets information related to the BNC acc.','Information'),(7,'.bncfind','<search parameters>','Currently, searches based on a custom algorithm. See more info for full details.','Information'),(8,'.bncabout','','Retrieves version/build information.','Information'),(9,'.bncrehash','[bncbot] [strings] [bncserver]','Reloads string tables and config. Specify arguments to only reload that type. (Only string table is useful.)','Bot Administration'),(10,'.bncoverride','','Generates a one-time-use reg-time-check override code. All usage logged.','Bot Administration'),(11,'.savedata','','Sends modified data stored in memory to SQL. Done every 5 minutes anyway.','Bot Administration'),(12,'.forcesavedata','','Sends all data stored in memory to SQL. Normally only done on shutdown.','Bot Administration'),(13,'realip','<nick>','Gets the IPs of all clients connected to the BNC user. Throws various errors if user is offline or if user is not BNC.','Information'),(14,'searchip','<CIDR-format IP or IP>','Finds all users with client IPs matching the given IP/netmask. For format info see More Info.','Information'),(15,'.bncbl','<add/del/show> <arg>','Manages the BNC\'s blacklist. Arguments provided vary based on subcommand.','Bot Administration'),(16,'.bncmove','<id or nick> <new server>','Moves a user from one server to another. Only use on accepted requests. Does not preserve settings.','Request Management'),(17,'.bncls','<time in days>','Shortcut for .bncfind lastseen=<[time]','Information'),(18,'.loggrep','[[offset][,result count]] <query>','Searches through the bot\'s logs for <query>. Searching provided by MySQL fulltext search in BOOLEAN mode.','Information'),(19,'.bnclistsuspend','','Lists all currently suspended BNC accounts.','Request Management'),(20,'.bncadd','<nick> <server>','Adds a new account to the BNC and the bot\'s database. Used when the user cannot request for themselves.','Request Management'),(21,'.serverlist','','Lists all BNC servers the bot is managing and their status','Server Management'),(22,'.serverenable','<server>','Re-enables a BNC server, i.e. the bot will try to connect to it again.','Server Management'),(23,'.serverdisable','<server>','Disables a BNC server. The bot will not try to connect to disabled servers, but disabling a server does not terminate the connection.','Server Management'),(24,'iplog','<IP or id or nick>','Finds all attach/detach information for the given user or IP.','Information');
/*!40000 ALTER TABLE `bncbot_help` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bncbot_log`
--

DROP TABLE IF EXISTS `bncbot_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bncbot_log` (
  `event_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'The ID of the event.',
  `event_text` text NOT NULL COMMENT 'The event body.',
  `event_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'The time at which the event was logged.',
  PRIMARY KEY (`event_id`),
  FULLTEXT KEY `event_text` (`event_text`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Log for commands/things done via bot.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bncbot_suspension`
--

DROP TABLE IF EXISTS `bncbot_suspension`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bncbot_suspension` (
  `suspend_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'A unique suspension ID.',
  `user_id` int(10) unsigned NOT NULL COMMENT 'The ID of the user that was suspended.',
  `suspend_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'The time at which the user was suspended.',
  `suspend_expiry` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'The time at which the suspension will expire.',
  PRIMARY KEY (`suspend_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bncbot_user_log`
--

DROP TABLE IF EXISTS `bncbot_user_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bncbot_user_log` (
  `event_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'The ID of the user event.',
  `bnc_user_id` int(10) unsigned NOT NULL COMMENT 'The ID of the user associated with the event.',
  `event_type` enum('attach','detach') NOT NULL COMMENT 'The type of the event.',
  `event_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'The time at which the event occurred.',
  `client_ip` varchar(15) NOT NULL COMMENT 'The IP of the client which attached/detached.',
  PRIMARY KEY (`event_id`),
  KEY `bnc_user_id` (`bnc_user_id`),
  CONSTRAINT `bnc_user_id` FOREIGN KEY (`bnc_user_id`) REFERENCES `bncbot_users` (`bnc_user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bncbot_users`
--

DROP TABLE IF EXISTS `bncbot_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bncbot_users` (
  `bnc_user_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'The ID of the bnc user.',
  `bnc_user_nick` varchar(40) NOT NULL COMMENT 'The main nick of the user that requested a BNC.',
  `bnc_user_hostmask` varchar(255) NOT NULL COMMENT 'The hostmask of the user at the time of request.',
  `bnc_time_requested` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The time of request.',
  `bnc_state` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'The current state of the request. See net.rizon.bncbot.UserDB.UserEntry.ApprovalState',
  `bnc_action_by` varchar(40) DEFAULT NULL COMMENT 'The nick of the user that performed the last action this BNC request.',
  `bnc_action_by_hostmask` varchar(255) DEFAULT NULL COMMENT 'The hostmask of the user that performed the last action this BNC request.',
  `bnc_action_time` timestamp NULL DEFAULT NULL COMMENT 'The last time an action was performed on this BNC request.',
  `bnc_action_reason` text COMMENT 'An optional reason for why the action was performed. Usually used only if bnc_state > 1.',
  `bnc_note` text COMMENT 'An optional note for user.',
  `bnc_server_id` int(9) unsigned NOT NULL DEFAULT '1' COMMENT 'The ID of the server the user exists on.',
  PRIMARY KEY (`bnc_user_id`),
  KEY `bnc_server_id` (`bnc_server_id`),
  CONSTRAINT `bncbot_users_ibfk_1` FOREIGN KEY (`bnc_server_id`) REFERENCES `bncbot_bncs` (`server_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Contains user entries/requests and associated data.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Final view structure for view `bnc_lastseen`
--

/*!50001 DROP TABLE IF EXISTS `bnc_lastseen`*/;
/*!50001 DROP VIEW IF EXISTS `bnc_lastseen`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = latin1 */;
/*!50001 SET character_set_results     = latin1 */;
/*!50001 SET collation_connection      = latin1_swedish_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`rizonbnc`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `bnc_lastseen` AS select `a`.`user_id` AS `user_id`,`a`.`event_time` AS `event_time`,`b`.`event_type` AS `event_type`,`b`.`ip` AS `ip`,`b`.`was_last_client` AS `was_last_client` from (`bnc_user_last_conn_event_time` `a` left join `bncbot_conn_log` `b` on(((`b`.`user_id` = `a`.`user_id`) and (`b`.`event_time` = `a`.`event_time`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `bnc_user_last_conn_event_time`
--

/*!50001 DROP TABLE IF EXISTS `bnc_user_last_conn_event_time`*/;
/*!50001 DROP VIEW IF EXISTS `bnc_user_last_conn_event_time`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`rizonbnc`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `bnc_user_last_conn_event_time` AS select `bncbot_conn_log`.`user_id` AS `user_id`,max(`bncbot_conn_log`.`event_time`) AS `event_time` from `bncbot_conn_log` group by `bncbot_conn_log`.`user_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-10-20 19:26:32
