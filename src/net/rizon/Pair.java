package net.rizon;

public class Pair<T1, T2> {
	public T1 first;
	public T2 second;

	public Pair(T1 first, T2 second) {
		this.first = first;
		this.second = second;
	}

	public static boolean same(Object o1, Object o2) {
		return o1 == null ? o2 == null : o1.equals(o2);
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Pair<?, ?>))
			return false;
		Pair<?, ?> p = (Pair<?, ?>) obj;
		return same(p.first, this.first) && same(p.second, this.second);
	}

	@Override
	public String toString() {
		return "Pair{" + first + ", " + second + "}";
	}
}
