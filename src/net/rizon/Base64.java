package net.rizon;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

public final class Base64 {
	private static final char[] alphabetC;
	private static final String alphabetS;

	static {
		alphabetS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
		alphabetC = alphabetS.toCharArray();
	}

	public static String encode(byte[] input) {
		int paddingCount = (3 - (input.length % 3)) % 3;
		byte[] padded = zeroPad(input, input.length + paddingCount);
		String output = "";

		for (int i = 0; i < padded.length; i += 3) {
			// ///////////////|-------i-------|------i+1------|------i+2------|
			// 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
			// ///////////////|-----j-----|----j+1----|----j+2----|----j+3----|
			int j = (byteToUByte(padded[i]) << 16) + (byteToUByte(padded[i + 1]) << 8) + byteToUByte(padded[i + 2]);
			output = output + alphabetC[(j >> 18) & 0x3F] + alphabetC[(j >> 12) & 0x3F] + alphabetC[(j >> 6) & 0x3F] + alphabetC[j & 0x3F];
		}

		return output.substring(0, output.length() - paddingCount) + "==".substring(0, paddingCount);
	}

	public static String encodeFromString(String input, Charset charset) {
		return encode(input.getBytes(charset));
	}

	public static String encodeFromString(String input) {
		return encodeFromString(input, Charset.forName("UTF-8"));
	}

	public static byte[] decode(String input) {
		char[] inputArr = input.toCharArray();
		byte[] decode = new byte[input.length() * 3 / 4];
		int decodePP = 0, padding = 0;

		for (int i = 0; i < inputArr.length; i++)
			if ((inputArr[i] != '=') && (alphabetS.indexOf(inputArr[i]) == -1))
				throw new IllegalArgumentException("Invalid base64 string passed to decode(String)");

		for (int i = 0; i < inputArr.length; i += 4) {
			int a = alphabetS.indexOf(inputArr[i]), b = alphabetS.indexOf(inputArr[i + 1]), c = alphabetS.indexOf(inputArr[i + 2]), d = alphabetS.indexOf(inputArr[i + 3]);
			int j = 0;

			j += a << 18;
			j += b << 12;
			if (c == -1)
				padding++;
			else
				j += c << 6;
			if (d == -1)
				padding++;
			else
				j += d;

			for (int k = 2; k >= 0; k--) {
				decode[decodePP++] = (byte) ((j >> (k * 8)) & 0xFF);
			}
		}

		byte[] output = new byte[decode.length - padding];
		System.arraycopy(decode, 0, output, 0, output.length);
		return output;
	}

	public static String decodeToString(String input, Charset charset) {
		return charset.decode(ByteBuffer.wrap(decode(input))).toString();
	}

	public static String decodeToString(String input) {
		return decodeToString(input, Charset.forName("UTF-8"));
	}
	
	private static short byteToUByte(byte input) {
		return (short) (input & 0xFF);
	}

	private static byte[] zeroPad(byte[] input, int desiredLength) {
		byte[] output = new byte[desiredLength];
		System.arraycopy(input, 0, output, 0, input.length);
		return output;
	}
}