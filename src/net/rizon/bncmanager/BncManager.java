package net.rizon.bncmanager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;

import net.rizon.DatabaseConnection;
import net.rizon.bncbot.BncBot;
import net.rizon.bncbot.UserDB;

public final class BncManager {
	private boolean initialized;
	private Thread checkerThread;

	private Hashtable<String, BncServer> serverConnections;

	private BncEventHandler serverEventDelegate;

	public final static String versionString = "BNC Manager Class: $Id$";

	public BncManager() {
		this.initialized = false;

		this.serverConnections = new Hashtable<String, BncServer>();
		this.serverEventDelegate = null;
	}

	public void init() throws Exception {
		Connection conn = DatabaseConnection.getConnection();
		PreparedStatement ps = conn.prepareStatement("SELECT * FROM `bncbot_bncs`;");
		ResultSet rs = ps.executeQuery();

		while (rs.next()) {
			String abbr = rs.getString("server_abbr").toUpperCase();
			BncServer s = new BncServer(rs.getInt("server_id"), rs.getString("server_address"),
				rs.getInt("server_port"), rs.getBoolean("server_ssl"), abbr,
				rs.getString("server_nick"), rs.getString("server_pass"),
				rs.getString("server_gecos"), rs.getString("server_ident"),
				rs.getString("server_user_server"));

			s.connect();
			this.serverConnections.put(abbr, s);
		}

		rs.close();
		ps.close();

		this.checkerThread = new BncServerChecker();
		this.checkerThread.start();
		this.initialized = true;
	}

	public void cleanUp() {
		this.checkerThread.interrupt();

		for (BncServer s : this.serverConnections.values()) {
			s.disconnect();
		}

		this.serverConnections.clear();
		this.initialized = false;
	}

	public BncServer getServer(String abbr) {
		if (!this.initialized)
			throw new RuntimeException("BncManager not init'd!");

		return this.serverConnections.get(abbr.toUpperCase());
	}

	public BncServer getUserServer(UserDB.UserEntry ue) {
		return this.getServer(ue.getBncServerAbbr());
	}

	public BncServer[] getAllServers() {
		if (!this.initialized)
			throw new RuntimeException("BncManager not init'd!");

		return this.serverConnections.values().toArray(new BncServer[0]);
	}

	public String getServerAbbrList() {
		StringBuilder sb = new StringBuilder();

		for (BncServer s : this.serverConnections.values()) {
			sb.append(s.getServerAbbr());
			sb.append(", ");
		}

		return sb.substring(0, sb.length() - 2);
	}

	public void setServerEventHandler(BncEventHandler onReconnect) {
		this.serverEventDelegate = onReconnect;
	}

	@Override
	public String toString() {
		return "BNC Manager Class: Tracking " + this.serverConnections.size() + " servers.";
	}

	private class BncServerChecker extends Thread {
		public BncServerChecker() {
			super(null, null, "BNC Server Sentinel");
		}

		@Override
		public void run() {
			try {
				Thread.sleep(10000);

				while (!this.isInterrupted() && BncBot.get().running) {
					for (BncServer s : BncManager.this.serverConnections.values()) {
						if (!s.isEnabled())
							continue;

						if (s.getFailureCount() > 3) {
							stdPrintln("BNC '" + s.getServerAbbr() + "' failed 3 times; disabling connection.");
							s.disable();
						} else if (!s.isConnected() && !s.isConnecting()) {
							if (serverEventDelegate != null)
								serverEventDelegate.onReconnect(s);

							s.connect(serverEventDelegate);
						}
					}

					Thread.sleep(5000);
				}
			} catch (InterruptedException e) {
			} finally {
				stdPrintln("BNC Server Sentinel thread terminated.");
			}
		}

		private synchronized void stdPrintln(String s) {
			String date = "";

			if (BncBot.get() != null) {
				DateFormat df = new SimpleDateFormat(BncBot.get().config.getProperty("timestampFormat", ""));
				date = df.format(new Date());
			}

			System.out.println(date + "[ BNC ] " + s);
		}
	}

	public interface BncEventHandler {
		void onReconnect(BncServer s);

		void onConnectionEstablished(BncServer s);
	}
}
