package net.rizon.bncmanager;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Stack;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.SocketFactory;

import net.rizon.Pair;
import net.rizon.TrustingSSLSocketFactory;
import net.rizon.bncbot.BncBot;
import net.rizon.bncbot.UserDB;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class BncServer {
	private final String hostname, abbr, nick, pass, gecos, ident, userServer;
	private boolean ssl;
	private int id, port;
	private int userCount;

	private Timer bgTasks;
	private Thread connectionThread;

	private Socket socket;
	private BufferedReader netIn;
	private PrintWriter netOut;

	private ArrayList<String> hostsResults;
	private ArrayList<Pair<String, String>> searchResults;
	private Hashtable<String, Integer> statsResults;

	private int adminReplyRequests;
	private Stack<String> adminReplyStack;

	private volatile boolean connecting, connected, enabled;
	private int failureCount;

	private HashSet<String> serverPing;

	private BncManager.BncEventHandler connectCallback;

	/**
	 * If true, we must send ZNC 1.0-compatible commands. Otherwise, we must
	 * use ZNC 0.2* commands.
	 */
	private boolean znc1 = false;

	public final static String versionString = "BNC Server Class: $Id$";

	public BncServer(int id, String hostname, int port, boolean ssl, String abbr, String nick, String pass, String gecos, String ident, String userServer) {
		this.id = id;
		this.hostname = hostname;
		this.port = port;
		this.ssl = ssl;
		this.abbr = abbr;
		this.nick = nick;
		this.pass = pass;
		this.gecos = gecos;
		this.ident = ident;
		this.userServer = userServer;

		hostsResults = new ArrayList<String>();
		searchResults = new ArrayList<Pair<String, String>>();
		statsResults = new Hashtable<String, Integer>();

		adminReplyRequests = 0;
		adminReplyStack = new Stack<String>();

		connected = false;
		enabled = true;
		failureCount = 0;

		serverPing = new HashSet<String>();
		connectCallback = null;
	}

	public synchronized void connect() {
		if (this.connected || !enabled)
			return;

		// Wait 3 seconds to allow the thread to die. Hopefully.
		if (this.connectionThread != null && this.connectionThread.isAlive()) {
			this.disconnect();
		}

		this.connectionThread = new ConnectionThread();
		this.connectionThread.start();
	}

	public synchronized void connect(BncManager.BncEventHandler callback) {
		this.connectCallback = callback;
		this.connect();
	}

	public synchronized void disconnect() {
		if (this.connectionThread == null || !this.connected || !enabled)
			return;

		// Interrupt and then wait until it signals us, or three seconds. Whatever comes first.
		this.connectionThread.interrupt();

		try {
			synchronized (this.connectionThread) {
				this.connectionThread.wait(3000);
			}
		} catch (InterruptedException e) {
		}

		this.connectionThread = null;
	}

	public void disable() {
		this.enabled = false;

		this.failureCount = 0;
	}

	public void enable() {
		this.enabled = true;
	}

	public void addUser(String user, String password) {
		final String adminuser = (znc1 ? "*controlpanel" : "*admin");

		if (znc1) {
			/* ZNC 1.0 won't simply let adduser overwrite users. Since
			 * .bncapprove after a .bncdelete for inactive accounts isn't
			 * exactly unheard of, we need to purge the old user.
			 */
			privmsg(adminuser, String.format("deluser %s", user));

			privmsg(adminuser, String.format("adduser %s %s", user, password));
			privmsg(adminuser, String.format("addnetwork %s Rizon", user));
			privmsg(adminuser, String.format("addserver %s Rizon %s", user, userServer));
		} else
			privmsg(adminuser, String.format("adduser %s %s %s", user, password, userServer));

		// Set options.
		privmsg(adminuser, String.format("set DCCBindHost %s false", user));
		privmsg(adminuser, String.format("set DenySetBindHost %s true", user));
		privmsg(adminuser, String.format("set DenyLoadMod %s true", user));
		//privmsg(adminuser, String.format("set BindHost %s 127.0.0.1", user));

		// Load modules.
		//privmsg("*admin", String.format("LoadModule %s away ThisIsSecure123", user));
		privmsg(adminuser, String.format("LoadModule %s chansaver", user));
		//privmsg("*admin", String.format("LoadModule %s force_stickychan #RizonBNC", user));
		privmsg(adminuser, String.format("LoadModule %s force_local_conn", user));
		privmsg(adminuser, String.format("LoadModule %s nickserv", user));
		privmsg(adminuser, String.format("LoadModule %s perform", user));
		privmsg(adminuser, String.format("LoadModule %s simple_away", user));

		this.userCount++;
	}

	public void delUser(String user) {
		privmsg("*admin", String.format("deluser %s", user));
		this.userCount--;
	}

	public void blockUser(String user, String reason) {
		privmsg("*blockuser_mod", String.format("block %s %s", user, reason));
	}

	public void unblockUser(String user) {
		privmsg("*blockuser_mod", String.format("unblock %s", user));
	}

	public void changePass(String user, String pass) {
		privmsg("*admin", String.format("set password %s %s", user, pass));
	}

	public String loadModule(String user, String modArgStr) {
		adminReplyRequests++;

		privmsg("*admin", String.format("LoadModule %s %s", user, modArgStr));

		long end = System.currentTimeMillis() + 2000;
		while (System.currentTimeMillis() < end && adminReplyStack.empty())
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
			}

		if (adminReplyStack.empty())
			return "nothing. Server timeout?";
		else
			return adminReplyStack.pop();
	}

	public String unloadModule(String user, String mod) {
		adminReplyRequests++;

		privmsg("*admin", String.format("UnloadModule %s %s", user, mod));

		long end = System.currentTimeMillis() + 2000;
		while (System.currentTimeMillis() < end && adminReplyStack.empty())
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
			}

		if (adminReplyStack.empty())
			return "nothing. Server timeout?";
		else
			return adminReplyStack.pop();
	}

	public boolean isConnected() {
		return this.connected;
	}

	public boolean isConnecting() {
		return this.connecting;
	}

	public boolean isEnabled() {
		return this.enabled;
	}

	public String[] getHosts(UserDB.UserEntry user) throws RuntimeException {
		synchronized (this.hostsResults) {
			this.hostsResults = new ArrayList<String>();
		}

		synchronized (this.hostsResults) {
			privmsg("*status", "listclients " + user.getNick());

			try {
				this.hostsResults.wait(5000);
			} catch (Exception e) {
				e.printStackTrace();
			}

			if (this.hostsResults.size() == 0)
				throw new RuntimeException("errorResponseTimedout");

			String[] hosts = new String[this.hostsResults.size()];
			for (int i = 0; i < this.hostsResults.size(); i++)
				hosts[i] = this.hostsResults.get(i);

			if (hosts[0].equals("noclients"))
				throw new RuntimeException("errorNotConnected");
			else if (hosts[0].equals("nouser"))
				throw new RuntimeException("errorNoSuchUser");
			else
				return hosts;
		}
	}

	@SuppressWarnings("unchecked")
	public ArrayList<Pair<String, String>> searchIP(String ip) {
		synchronized (this.searchResults) {
			this.searchResults = new ArrayList<Pair<String, String>>();
		}

		synchronized (this.searchResults) {
			privmsg("*lastseen", "searchipjson " + ip);

			try {
				this.searchResults.wait(3000);
			} catch (Exception e) {
				e.printStackTrace();
			}

			if (this.searchResults.size() == 0)
				return null;
			else if (this.searchResults.get(0).first.equals("***ERROR***"))
				throw new RuntimeException(this.searchResults.get(0).second);
			else
				return (ArrayList<Pair<String, String>>) this.searchResults.clone();
		}
	}

	public int getUserCount() {
		return this.userCount;
	}

	public int[] getStats() {
		synchronized (this.statsResults) {
			this.statsResults = new Hashtable<String, Integer>();
		}

		synchronized (this.statsResults) {
			privmsg("*status", "userstats");

			try {
				this.statsResults.wait(3000);
			} catch (Exception e) {
				e.printStackTrace();
			}

			if (this.statsResults.size() == 0)
				return null;
			else
				return new int[] { this.statsResults.get("totalusers"), this.statsResults.get("online"), this.statsResults.get("totalconnected") };
		}
	}

	public String getServerHostname() {
		return this.hostname;
	}

	public String getServerAbbr() {
		return this.abbr;
	}

	public int getFailureCount() {
		return this.failureCount;
	}

	public void incFailureCount() {
		this.failureCount++;
	}

	public void resetFailureCount() {
		this.failureCount = 0;
	}

	public int getId() {
		return this.id;
	}

	@Override
	public String toString() {
		return String.format("(%s) %s:%s", this.abbr, this.hostname, this.port);
	}

	private void privmsg(String target, String msg) {
		if (msg.length() > 400) {
			for (int i = 400; i > 0; i--) {
				if (msg.substring(i, i + 1).matches("\\s")) {
					doPrivmsg(target, msg.substring(0, i));
					privmsg(target, msg.substring(i));
					return;
				}
			}
		} else {
			doPrivmsg(target, msg);
		}
	}

	private void doPrivmsg(String target, String line) {
		send("PRIVMSG " + target + " :" + line);
	}

	private void send(String line) {
		formatOut(line);
		netOut.println(line);
	}

	private void formatOut(String s) {
		if (BncBot.get().verbose)
			stdPrintln("-> " + s);
	}

	private void formatIn(String s) {
		if (BncBot.get().verbose)
			stdPrintln("<- " + s);
	}

	private synchronized void stdPrintln(String s) {
		String date = "";

		if (BncBot.get() != null) {
			DateFormat df = new SimpleDateFormat(BncBot.get().config.getProperty("timestampFormat", ""));
			date = df.format(new Date());
		}

		System.out.println(date + "[ BNC-" + this.abbr + " ] " + s);
	}

	private class PingTimer extends TimerTask {
		@Override
		public void run() {
			if (serverPing.size() > 0) {
				stdPrintln("ZNC ping timeout. Closing socket.");

				try {
					send("QUIT :Ping timeout (9001 seconds)");

					disconnect();
					socket.close();
				} catch (Exception e) {
					e.printStackTrace();
				}

				serverPing.clear();
				bgTasks.cancel();

				return;
			}

			String pingId = "" + System.currentTimeMillis() / 1000;

			serverPing.add(pingId);
			send("PING " + pingId);
		}
	}

	private class ConnectionThread extends Thread {
		public ConnectionThread() {
			super(null, null, "bnc-" + BncServer.this.abbr.toLowerCase() + "-net");
			this.setName(this.getName() + "-" + this.getId());
		}

		@Override
		public void run() {
			try {
				stdPrintln("Connecting to BNC server...");

				BncServer.this.connecting = true;

				if (BncServer.this.ssl) {
					socket = TrustingSSLSocketFactory.getInstance().createSocket(BncServer.this.hostname, BncServer.this.port, 1024 * 1024);
				} else {
					socket = SocketFactory.getDefault().createSocket();
					socket.setReceiveBufferSize(1024 * 1024);
					socket.connect(new InetSocketAddress(BncServer.this.hostname, BncServer.this.port));
				}

				BncServer.this.connecting = false;
				BncServer.this.connected = true;

				BncServer.this.netOut = new PrintWriter(socket.getOutputStream(), true);

				send("PASS " + BncServer.this.pass);
				send("USER " + BncServer.this.ident + " 1 * :" + BncServer.this.gecos);
				send("NICK " + BncServer.this.nick);

				BncServer.this.bgTasks = new Timer("bnc-timer", true);
				BncServer.this.bgTasks.schedule(new PingTimer(), 60000, 60000);

				stdPrintln("BNC connection established.");

				if(BncServer.this.connectCallback != null) {
					BncServer.this.connectCallback.onConnectionEstablished(BncServer.this);
					BncServer.this.connectCallback = null;
				}

				// Query admin module so we can adjust the commands we send
				BncServer.this.privmsg("*admin", "test");

				BncServer.this.privmsg("*status", "userstats");

				// TODO - Fix so that scan.nextLine() does not have to return for thread to exit.
				netIn = new BufferedReader(new InputStreamReader(socket.getInputStream()), 1024 * 512);
				Pattern ccStripper = Pattern.compile("[\u0002\u001F\u000F]");
				Pattern colorStripper = Pattern.compile("\u0003[0-9]{1,2}(,[0-9]{1,2})?");
				Pattern hostsStartParse = Pattern.compile("^:\\*status!.*@.* PRIVMSG .* :\\| Host *\\|");
				Pattern hostsParse = Pattern.compile("^:\\*status!.*@.* PRIVMSG .* :\\| ([0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}) \\|");
				Pattern adminReply = Pattern.compile("^:\\*admin!.*@.* PRIVMSG .* :(.*)$");
				Pattern notifyConnectPattern = Pattern.compile("^:\\*status!.*@.* NOTICE [^:]* :\\*{3} (.*) (attach|detach)ed \\((from|gone:) " +
					"([0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3})\\)( \\([0-9]+ clients left\\))?");
				String line, words[];
				while (BncBot.get().running && socket.isConnected()) {// && !Thread.interrupted()) {
					line = netIn.readLine();
					if (line == null)
						break;

					line = ccStripper.matcher(line).replaceAll("");
					line = colorStripper.matcher(line).replaceAll("");
					line = line.replace("\u0003", "");

					if (line.indexOf("JSON:") == -1) // Don't print JSON data fully.
						formatIn(line);

					words = line.split("\\s+");

					// Respond to ping.
					if (line.startsWith("PING ") && words.length > 1)
						send("PONG " + words[1]);

					// Handle pong messages.
					else if (line.startsWith("PONG ") && words.length > 1) {
						if (serverPing.contains(words[1]))
							serverPing.remove(words[1]);
					}
					else if (words.length > 3 && words[1].equals("PONG")) {
						if (serverPing.contains(words[3]))
							serverPing.remove(words[3]);
					}

					// Deal with failure to contact *admin -- meaning we're on 1.0.
					else if (line.startsWith(":*status") && words.length == 7
						&& words[3].equals(":No") && words[6].equals("[admin]"))
						znc1 = true;

					// Handle connection event log.
					Matcher connectMatcher = notifyConnectPattern.matcher(line);
					if(connectMatcher.matches()) {
						String username = connectMatcher.group(1);
						String verb = connectMatcher.group(2);
						String ip = connectMatcher.group(4);
						boolean lastClient = connectMatcher.group(5) == null;

						try {
							BncBot.get().database.logConnectEvent(username, verb, ip, lastClient);
						}
						catch (Throwable ex) {
							System.out.println("Error processing connect event for " + username + ": " + line);
							ex.printStackTrace();
						}
						continue;
					}

					// Handle JSON stuff.
					int jsonPos = line.indexOf("JSON:");
					if (jsonPos > -1 || line.contains("userstats")) {
						int dataStart = (jsonPos > -1 ? jsonPos + 5 : line.indexOf("{"));

						formatIn("JSON (truncated): " + line.substring(dataStart, (line.length() - dataStart) > 40 ? (dataStart + 40) : line.length()));

						//if (jsonPos > -1) {
						try {
							//JSONObject jo = new JSONObject(line.substring(jsonPos + 5));
							JSONObject jo = new JSONObject(line.substring((jsonPos > -1 ? jsonPos + 5 : line.indexOf("{"))));

							// IP search reply
							if (jo.optBoolean("searchipreply") && jo.has("data")) {
								synchronized (BncServer.this.searchResults) {
									JSONArray ja = jo.getJSONArray("data");

									for (int i = 0; i < ja.length(); i++)
										BncServer.this.searchResults.add(new Pair<String, String>(ja.getJSONObject(i).getString("user"), ja.getJSONObject(i).getString("ip")));

									BncServer.this.searchResults.notifyAll();
								}
							}

							// IP search error
							else if (jo.optBoolean("searchipreply") && jo.has("error")) {
								synchronized (BncServer.this.searchResults) {
									BncServer.this.searchResults.add(new Pair<String, String>("***ERROR***", jo.getString("error")));
									BncServer.this.searchResults.notifyAll();
								}
							}

							// BNC stats reply
							else if (jo.optBoolean("userstats") && jo.has("totalusers") && jo.has("online") && jo.has("totalconnected")) {
								synchronized (BncServer.this.statsResults) {
									BncServer.this.statsResults.put("totalusers", jo.getInt("totalusers"));
									BncServer.this.statsResults.put("online", jo.getInt("online"));
									BncServer.this.statsResults.put("totalconnected", jo.getInt("totalconnected"));
									BncServer.this.userCount = jo.getInt("totalusers");
									BncServer.this.statsResults.notifyAll();
								}
							}
						} catch (JSONException jsone) {
							stdPrintln("Error while parsing JSON data from the BNC server: " + jsone.getMessage());
						}
					}

					// Catch admin module output if we want it.
					if (adminReplyRequests > 0) {
						Matcher adminReplyM = adminReply.matcher(line);

						if (adminReplyM.matches()) {
							adminReplyRequests--;
							adminReplyStack.push(adminReplyM.group(1));
						}
					}

					// Parse hosts output.
					Matcher hostsStartM = hostsStartParse.matcher(line);
					if (hostsStartM.matches()) {
						formatIn(netIn.readLine()); // Skip the divider.

						String line2;
						while ((line2 = netIn.readLine()) != null) {
							formatIn(line2);

							// Match for a host and also the end.
							Matcher hostsM = hostsParse.matcher(line2);
							synchronized (BncServer.this.hostsResults) {
								if (hostsM.matches()) {
									BncServer.this.hostsResults.add(hostsM.group(1));
								} else {
									BncServer.this.hostsResults.notifyAll();
									break;
								}
							}
						}
					} else if (line.contains("No clients are connected")) {
						synchronized (BncServer.this.hostsResults) {
							BncServer.this.hostsResults.add("noclients");
							BncServer.this.hostsResults.notifyAll();
						}
					} else if (line.contains("No such user")) {
						synchronized (BncServer.this.hostsResults) {
							BncServer.this.hostsResults.add("nouser");
							BncServer.this.hostsResults.notifyAll();
						}
					}
				}

				BncServer.this.netIn.close();
				BncServer.this.netOut.close();
				BncServer.this.socket.close();

				stdPrintln("BNC connection closed. Network thread terminating.");
			} catch (SocketException se) {
				stdPrintln("Network error caused BNC connection to close. Network thread terminating. Exception detail follows.");
				se.printStackTrace();

				failureCount++;
			} catch (Exception e) {
				e.printStackTrace();
				stdPrintln("Network thread terminating due to exception.");

				failureCount++;
			} finally {
				BncServer.this.connected = BncServer.this.connecting = false;
				BncServer.this.bgTasks.cancel();

				synchronized (Thread.currentThread()) {
					Thread.currentThread().notifyAll();
				}
			}
		}

		private synchronized void stdPrintln(String s) {
			String date = "";

			if (BncBot.get() != null) {
				DateFormat df = new SimpleDateFormat(BncBot.get().config.getProperty("timestampFormat", ""));
				date = df.format(new Date());
			}

			System.out.println(date + "[ BNC-NET ] " + BncServer.this.abbr + ": " + s);
		}
	}
}
