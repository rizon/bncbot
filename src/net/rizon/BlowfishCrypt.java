package net.rizon;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Iterator;

import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import net.rizon.bncbot.BncBot;

import com.martiansoftware.jsap.FlaggedOption;
import com.martiansoftware.jsap.JSAP;
import com.martiansoftware.jsap.JSAPException;
import com.martiansoftware.jsap.JSAPResult;
import com.martiansoftware.jsap.Switch;
import com.martiansoftware.jsap.UnflaggedOption;
import com.martiansoftware.jsap.stringparsers.StringStringParser;

public final class BlowfishCrypt {
	private static final BlowfishCrypt instance = new BlowfishCrypt();

	private BlowfishCrypt() {
	}

	public byte[] encrypt(byte[] input, byte[] key) throws Exception {
		return crypt(input, key, Cipher.ENCRYPT_MODE);
	}

	public byte[] encrypt(String input, String key) throws Exception {
		return crypt(input.getBytes("UTF-8"), key.getBytes("UTF-8"), Cipher.ENCRYPT_MODE);
	}

	public String encryptToBase64(byte[] input, byte[] key) throws Exception {
		return Base64.encode(encrypt(input, key));
	}

	public String encryptToBase64(String input, String key) throws Exception {
		return Base64.encode(encrypt(input, key));
	}

	public byte[] decrypt(byte[] input, byte[] key) throws Exception {
		return crypt(input, key, Cipher.DECRYPT_MODE);
	}

	public byte[] decrypt(String input, String key) throws Exception {
		return crypt(input.getBytes("UTF-8"), key.getBytes("UTF-8"), Cipher.DECRYPT_MODE);
	}

	public byte[] decryptFromBase64(String input, String key) throws Exception {
		return decrypt(Base64.decode(input), key.getBytes("UTF-8"));
	}

	public byte[] decryptFromBase64(String input, byte[] key) throws Exception {
		return decrypt(Base64.decode(input), key);
	}

	public String decryptFromBase64AsString(String input, String key) throws Exception {
		return Charset.forName("UTF-8").decode(ByteBuffer.wrap(decrypt(Base64.decode(input), key.getBytes("UTF-8")))).toString();
	}

	public String decryptFromBase64AsString(String input, byte[] key) throws Exception {
		return Charset.forName("UTF-8").decode(ByteBuffer.wrap(decrypt(Base64.decode(input), key))).toString();
	}

	private byte[] crypt(byte[] input, byte[] key, int mode) throws Exception {
		SecretKeySpec skeySpec = new SecretKeySpec(key, "Blowfish");
		IvParameterSpec ivSpec = new IvParameterSpec(deriveIv(key));
		Cipher cipher = Cipher.getInstance("Blowfish/CBC/PKCS5Padding");
		ByteArrayOutputStream bos;
		CipherOutputStream cos;
		byte[] output;

		cipher.init(mode, skeySpec, ivSpec);

		bos = new ByteArrayOutputStream();
		cos = new CipherOutputStream(bos, cipher);

		cos.write(input);
		cos.close();
		output = bos.toByteArray();
		bos.close();

		return output;
	}

	private byte[] deriveIv(byte[] key) {
		// 8-byte IV base. 3.14159265358979
		byte[] iv = new byte[] { 0x30, 0x14, 0x15, (byte) 0x92, 0x65, 0x35, (byte) 0x89, 0x79 };

		// XOR the key into the IV block.
		int ivP = 0;
		for (int kP = 0; kP < key.length; kP++) {
			iv[ivP++] ^= key[kP];
			ivP = ivP % 8;
		}

		// Voodoo.
		for (int i = 0; i < 3; i++) {
			// Randomize the IV.
			iv[0] <<= 3;
			iv[1] >>= 2;
			iv[2] ^= iv[1] + iv[2];
			iv[3] &= iv[2];
			iv[4] |= iv[0];
			iv[5] ^= iv[2] - iv[4];
			iv[6] &= iv[3] + iv[5];
			iv[7] ^= iv[0] + iv[2] + iv[4];

			// XOR it again with the key.
			ivP = 0;
			for (int kP = 0; kP < key.length; kP++) {
				iv[ivP++] ^= key[kP];
				ivP = ivP % 8;
			}
		}

		return new byte[] { iv[1], iv[4], iv[2], iv[7], iv[0], iv[3], iv[6], iv[5] };
	}

	public static BlowfishCrypt getInstance() {
		return BlowfishCrypt.instance;
	}

	public static void main(String[] args) {
		try {
			JSAP jsap = new JSAP();

			jsap.registerParameter(new Switch("encrypt").setShortFlag('e').setLongFlag("encrypt").setHelp("Set program to encrypt mode. (Mutually exclusive with decrypt.)"));
			jsap.registerParameter(new Switch("decrypt").setShortFlag('d').setLongFlag("decrypt").setHelp("Set program to decrypt mode. (Mutually exclusive with encrypt.)"));
			jsap.registerParameter(new Switch("help").setShortFlag('h').setLongFlag("help").setHelp("Show this help information."));
			jsap.registerParameter(new FlaggedOption("key").setStringParser(StringStringParser.getParser()).setRequired(true).setShortFlag('k').setLongFlag("key").setHelp("Key to encrypt with."));
			jsap.registerParameter(new UnflaggedOption("text").setStringParser(StringStringParser.getParser()).setRequired(true).setGreedy(true).setHelp("Text to encrypt/decrypt."));

			JSAPResult cmdconf = jsap.parse(args);

			if (cmdconf.success() && !cmdconf.getBoolean("help")) {
				if ((cmdconf.getBoolean("encrypt") && cmdconf.getBoolean("decrypt")) || !(cmdconf.getBoolean("encrypt") || cmdconf.getBoolean("decrypt"))) {
					System.err.println();
					System.err.println("Error: You must specify exactly one of either -e or -d");
				} else {
					try {
						if (cmdconf.getBoolean("encrypt"))
							System.out.println(BlowfishCrypt.getInstance().encryptToBase64(cmdconf.getString("text"), cmdconf.getString("key")));
						else
							System.out.println(BlowfishCrypt.getInstance().decryptFromBase64AsString(cmdconf.getString("text"), cmdconf.getString("key")));
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			} else {
				System.err.println();

				for (@SuppressWarnings("rawtypes")
				Iterator iter = cmdconf.getErrorMessageIterator(); iter.hasNext();) {
					System.err.println("Error: " + iter.next());
				}

				System.err.println();
				System.err.println("Usage: java " + BncBot.class.getName());
				System.err.println("            " + jsap.getUsage());
				System.err.println();
				System.err.println(jsap.getHelp());
				System.exit(1);
			}
		} catch (JSAPException e) {
			System.err.println("Error parsing command-line arguments: " + e.getMessage());
			e.printStackTrace();
		}
	}
}
