package net.rizon.bncbot;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.URL;
import java.security.Provider;
import java.security.Security;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Properties;
import java.util.Queue;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;

import net.rizon.BlowfishCrypt;
import net.rizon.DatabaseConnection;
import net.rizon.TrustingSSLSocketFactory;
import net.rizon.bncbot.UserDB.UserEntry;
import net.rizon.bncmanager.BncManager;
import net.rizon.bncmanager.BncManager.BncEventHandler;
import net.rizon.bncmanager.BncServer;

import com.martiansoftware.jsap.FlaggedOption;
import com.martiansoftware.jsap.JSAP;
import com.martiansoftware.jsap.JSAPException;
import com.martiansoftware.jsap.JSAPResult;
import com.martiansoftware.jsap.Switch;
import com.martiansoftware.jsap.stringparsers.FileStringParser;
import com.martiansoftware.jsap.stringparsers.StringStringParser;
import com.maxmind.geoip.LookupService;
import com.sun.crypto.provider.SunJCE;

public class BncBot {
	private static BncBot instance;
	private static Thread instanceThread;

	private static byte[] blowfishKey;

	private Socket socket;
	private OutputStream netOutStream;
	private PrintWriter netOut;
	private InputStream netInStream;
	private BufferedReader netIn;

	private Timer bgTasks;

	private Hashtable<String, String> whyLookup = new Hashtable<String, String>();
	private Hashtable<String, Date> regTimeLookup = new Hashtable<String, Date>();
	private Hashtable<String, Boolean> identifiedLookup = new Hashtable<String, Boolean>();
	private Hashtable<String, Boolean> operLookup = new Hashtable<String, Boolean>();

	private HashSet<String> serverPing = new HashSet<String>();

	private Queue<MemoQueueItem> memoQueue = new LinkedList<MemoQueueItem>();

	public UserDB database;
	public BncManager bncManager;
	public LookupService geoip;

	public boolean debug;
	public boolean verbose;

	public boolean running = true;
	private boolean cleanedUp = false;

	public final Properties config = new Properties();
	public File configFile;
	public final Properties stringTable = new Properties();
	public File stringTableFile;
	public File waConfigFile;

	public final static String versionString = "Main bot class: $Id$";

	// ===========================================

	// Constructor
	private BncBot(JSAPResult cmdconf) throws FileNotFoundException, IOException {
		// Set flags.
		this.verbose = cmdconf.getBoolean("verbose");
		this.debug = cmdconf.getBoolean("debug");

		// Define stuff.
		if (this.debug) {
			// System.setProperty("javax.net.debug", "all");
		}

		// Load config.
		this.configFile = cmdconf.getFile("botPropsFile");
		this.loadConfig();

		// Load strings.
		this.stringTableFile = new File("strings." + config.getProperty("stringsLanguage") + ".properties");
		if (!this.stringTableFile.exists())
			this.stringTableFile = new File("strings.properties");
		if (!this.stringTableFile.exists())
			throw new FileNotFoundException("No string table found.");
		this.loadStringTable();

		// Check to see that the key is specified if we're encrypting out passwords.
		if (!cmdconf.contains("blowfishKey") && Boolean.valueOf(config.getProperty("blowfishPasswords"))) {
			throw new RuntimeException("If using blowfish to encrypt passwords, the key must be specified on the command line.");
		}

		// Decrypt passwords.
		if (Boolean.valueOf(config.getProperty("blowfishPasswords"))) {
			try {
				stdPrintln("Decrypting passwords...");

				// Decrypt passwords in place.
				BncBot.blowfishKey = cmdconf.getString("blowfishKey").getBytes("UTF-8");
				BlowfishCrypt crypt = BlowfishCrypt.getInstance();

				if (config.getProperty("pass") != "")
					config.setProperty("pass", crypt.decryptFromBase64AsString(config.getProperty("pass"), BncBot.blowfishKey));
				if (config.getProperty("nickservPass") != "")
					config.setProperty("nickservPass", crypt.decryptFromBase64AsString(config.getProperty("nickservPass"), BncBot.blowfishKey));
				if (config.getProperty("sqlPass") != "")
					config.setProperty("sqlPass", crypt.decryptFromBase64AsString(config.getProperty("sqlPass"), BncBot.blowfishKey));
				if (config.getProperty("keystorePass") != "")
					config.setProperty("keystorePass", crypt.decryptFromBase64AsString(config.getProperty("keystorePass"), BncBot.blowfishKey));
			} catch (Exception e) {
				throw new RuntimeException("Error decrypting passwords: " + e.getMessage(), e);
			}
		}

		// Set WebAdmin config file.
		this.waConfigFile = cmdconf.getFile("waPropsFile");

		// Initialize BNC manager.
		this.bncManager = new BncManager();

		// Grab and load GeoIP database.
		this.getGeoIP();
		//this.geoip = new LookupService("GeoIP.dat", LookupService.GEOIP_INDEX_CACHE);
	}

	// Entry point.
	public static void main(String[] args) {
		try {
			// Add Security providers.
			Provider sunJCE = new SunJCE();
			Security.addProvider(sunJCE);

			// Parse configuration.
			JSAPResult cmdconf = BncBot.parseCommandLine(args);

			if (cmdconf == null) {
				System.exit(1); // FAILURE.
			} else {
				boolean allControllingBoolean = true;

				// Add shutdown handler before looping.
				Runtime.getRuntime().addShutdownHook(new Thread() {
					@Override
					public void run() {
						try {
							System.err.println("Caught signal...halting.");

							BncBot.instance.running = false;
							BncBot.instance.socket.close();
							BncBot.instanceThread.interrupt();

							BncBot.instance.cleanUp();
						} catch (Exception e) {
							System.err.println("Unable to interrupt main thread: " + e.getMessage());
						}
					}
				});

				while (allControllingBoolean)
					try {
						// Instantiate.
						BncBot.instance = new BncBot(cmdconf);
						DatabaseConnection.setProps(instance.config);

						// Initialize commands.
						CommandManager.getInstance().reset();

						instance.stdPrintln("Initialization completed...spawning main loop thread.");

						// Set the thread running the loop to this thread...
						BncBot.instanceThread = Thread.currentThread();

						// ...then run the loop.
						BncBot.instance.botLoop();

						// If we returned then there's a mess. Clean this mess up.
						BncBot.instance.cleanUp();

						if (!BncBot.instance.running) {
							allControllingBoolean = false;
						}

						System.err.println("Main loop halted...waiting 5 seconds.");
						Thread.sleep(5000);
					} catch (FileNotFoundException fnfe) {
						System.err.println("Error loading config: " + fnfe.getMessage());
						fnfe.printStackTrace();
					} catch (IOException ioe) {
						System.err.println("Error loading config: " + ioe.getMessage());
						ioe.printStackTrace();
					} catch (RuntimeException re) {
						re.printStackTrace();
					} catch (InterruptedException ie) {
						ie.printStackTrace();
					}

				System.exit(0);
			}
		} catch (JSAPException e) {
			System.err.println("Error parsing command-line arguments: " + e.getMessage());
			e.printStackTrace();
		}

		System.exit(1);
	}

	// Get instance.
	public static BncBot get() {
		return BncBot.instance;
	}

	// Separate routine for parsing the command line. Reduces mess.
	private static JSAPResult parseCommandLine(String[] args) throws JSAPException {
		JSAP jsap = new JSAP();

		jsap.registerParameter(new Switch("verbose").setShortFlag('v').setLongFlag("verbose").setHelp("Enable verbose output."));
		jsap.registerParameter(new Switch("debug").setShortFlag('d').setLongFlag("debug").setHelp("Enable debug information output."));
		jsap.registerParameter(new Switch("help").setShortFlag('h').setLongFlag("help").setHelp("Show this help information."));
		jsap.registerParameter(new FlaggedOption("botPropsFile").setStringParser(FileStringParser.getParser().setMustBeFile(true).setMustExist(true)).setRequired(true).setDefault("bncbot.properties").setShortFlag('c').setLongFlag("botprops").setHelp("File to read configuration from."));
		jsap.registerParameter(new FlaggedOption("waPropsFile").setStringParser(FileStringParser.getParser().setMustBeFile(true).setMustExist(true)).setRequired(true).setDefault("webadmin.properties").setShortFlag(JSAP.NO_SHORTFLAG).setLongFlag("webprops").setHelp("File to read WebAdmin configuration from."));
		jsap.registerParameter(new FlaggedOption("blowfishKey").setStringParser(StringStringParser.getParser()).setShortFlag('k').setLongFlag("key").setHelp("The blowfish key to be used if configuration is blowfish-encrypted."));

		JSAPResult retVal = jsap.parse(args);
		if (!retVal.success()) {
			System.err.println();

			for (@SuppressWarnings("rawtypes")
			Iterator iter = retVal.getErrorMessageIterator(); iter.hasNext();) {
				System.err.println("Error: " + iter.next());
			}

			System.err.println();
			System.err.println("Usage: java " + BncBot.class.getName());
			System.err.println("            " + jsap.getUsage());
			System.err.println();
			System.err.println(jsap.getHelp());

			return null;
		} else {
			return retVal;
		}
	}

	// =============================================

	// Main bot loop.
	public void botLoop() {
		try {
			this.database = new UserDB();

			this.stdPrintln("Connecting to IRC server...");
			this.socket = TrustingSSLSocketFactory.getInstance().createSocket(this.config.getProperty("server"), Integer.parseInt(this.config.getProperty("port")));
			this.netOutStream = this.socket.getOutputStream();
			this.netOut = new PrintWriter(this.netOutStream, true);

			// Send stuff at beginning.
			if (!this.config.getProperty("pass").equals(""))
				this.send("PASS :" + this.config.getProperty("pass"));
			this.send("NICK " + this.config.getProperty("nick"));
			this.send("USER " + this.config.getProperty("ident") + " 1 * :" + this.config.getProperty("gecos"));

			this.stdPrintln("IRC connection established.");

			// Set up timer tasks.
			bgTasks = new Timer();
			bgTasks.scheduleAtFixedRate(new DatabaseDaemon(), 300000, 300000);
			bgTasks.scheduleAtFixedRate(new UnsuspendTimer(), 10000, 300000);
			bgTasks.schedule(new PingTimer(), 60000, 60000);
			bgTasks.schedule(new MemoSendTimer(), 4000, 4000);

			// Connect to BNC.
			bncManager.init();
			bncManager.setServerEventHandler(new BncEventHandler() {
				@Override
				public void onReconnect(BncServer s) {
					privmsg(config.getProperty("adminChannel"), getString("adminBncServerReconnecting", s.toString()));
				}

				@Override
				public void onConnectionEstablished(BncServer s) {
					privmsg(config.getProperty("adminChannel"), getString("adminBncServerReconnected", s.toString()));
				}
			});

			// Set up main input loop.
			this.netInStream = this.socket.getInputStream();
			this.netIn = new BufferedReader(new InputStreamReader(this.netInStream));
			String line, words[], userlookup = null;
			Pattern ccStripper = Pattern.compile("[\u0002\u001F\u000F]");
			Pattern colorStripper = Pattern.compile("\u0003[0-9]{1,2}(,[0-9]{1,2})?");
			Pattern whyPattern = Pattern.compile("([A-Z0-9`_^|\\[\\]\\-]*) has [A-Z]* access to #[A-Za-z-]*. Reason: .*\\. Main nick: (.*)", Pattern.CASE_INSENSITIVE);
			Pattern userIs = Pattern.compile("([A-Z0-9`_^|\\[\\]\\-]*) is.*", Pattern.CASE_INSENSITIVE);
			Pattern regPattern = Pattern.compile("Time registered: +(.*)$", Pattern.CASE_INSENSITIVE);
			DateFormat dateParser = new SimpleDateFormat("MMM dd hh:mm:ss yyyy z");

			// TODO - Fix exiting.
			// Right now, nextLine() must return before the program exits (i.e. shutdown via command)
			while (running && !Thread.interrupted() && this.socket.isConnected()) {
				line = netIn.readLine();
				if (line == null)
					break;

				line = ccStripper.matcher(line).replaceAll("");
				line = colorStripper.matcher(line).replaceAll("");
				line = line.replace("\u0003", "");

				formatIn(line);

				words = line.split("\\s+");

				// Handle ping messages.
				if (line.startsWith("PING ") && words.length > 1)
					this.send("PONG " + words[1]);

				// Handle pong messages.
				if (words.length == 4 && words[1].equals("PONG")) {
					String id = words[3].substring(1);

					if (serverPing.contains(id))
						serverPing.remove(id);
				}

				// Look for end of MOTD.
				else if (words[1].equals("376")) {
					new Thread() {
						@Override
						public void run() {
							this.setName("Autojoin Thread");

							if (!config.getProperty("nickservPass").equals(""))
								privmsg("NickServ", "IDENTIFY " + config.getProperty("nickservPass"));
							privmsg("ChanServ", "INVITE " + config.getProperty("adminChannel"));

							try {
								Thread.sleep(1000); // Sleep to allow NickServ to process our IDENT.
							} catch (Exception e) {
							}
							send("JOIN " + config.getProperty("adminChannel"));
							send("JOIN " + config.getProperty("whyChannel"));

							send("MODE " + config.getProperty("nick") + " +Gp");

							// Do the autojoin stuff.
							String[] autojoin = config.getProperty("autojoinChannels").split(",");
							for (String chan : autojoin)
								send("JOIN " + chan);
						}
					}.run();
				}

				// Look for WHOIS reply for identified.
				else if (words[1].equals("307")) {
					synchronized (this.identifiedLookup) {
						this.identifiedLookup.put(words[3], true);
					}
				}

				// Look for end of WHOIS to signal unidentified.
				// Only signal when WHOIS is over, to ensure that things aren't added twice.
				else if (words[1].equals("318")) {
					synchronized (this.identifiedLookup) {
						if (!this.identifiedLookup.containsKey(words[3]))
							this.identifiedLookup.put(words[3], false);

						this.identifiedLookup.notifyAll();
					}
				}

				// Look for WHO reply for OPER.
				else if (words.length >= 11 && words[1].equals("352")) {
					synchronized (this.operLookup) {
						if (words[8].contains("*")) {
							this.operLookup.put(words[7].toLowerCase(), true);
						} else {
							this.operLookup.put(words[7].toLowerCase(), false);
						}
						this.operLookup.notifyAll();
					}
				}

				// Look for INFO responses (requires special treatment.)
				else if (words[0].split("!")[0].equals(":NickServ")) {
					String message = "";
					for (int i = 3; i < words.length; i++)
						message += ((i == 3) ? words[i].substring(1) : words[i]) + " ";
					message = message.trim();

					Matcher ui = userIs.matcher(message), rp = regPattern.matcher(message);
					if (ui.matches() && ui.groupCount() == 1) {
						userlookup = ui.group(1);
					} else if (rp.matches() && rp.groupCount() == 1 && userlookup != null) {
						synchronized (this.regTimeLookup) {
							this.regTimeLookup.put(userlookup, dateParser.parse(rp.group(1)));
							this.regTimeLookup.notifyAll();
						}
						userlookup = null;
					}
				}

				// Look for WHY.
				else if (words[0].split("!")[0].equals(":ChanServ")) {
					String message = "";
					for (int i = 3; i < words.length; i++)
						message += ((i == 3) ? words[i].substring(1) : words[i]) + " ";
					message = message.trim();

					Matcher m = whyPattern.matcher(message);
					if (m.matches() && m.groupCount() == 2) {
						synchronized (this.whyLookup) {
							this.whyLookup.put(m.group(1).trim(), m.group(2).trim());
							this.whyLookup.notifyAll();
						}
					}
				}

				else if (words[1].equals("PRIVMSG")) {
					new MessageProc(line).start();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public synchronized void cleanUp() throws IOException {
		if (!this.cleanedUp) {
			// Save database.
			bgTasks.cancel();
			database.saveData(true);

			// Close BNC connections.
			if (bncManager != null)
				bncManager.cleanUp();

			// Close GeoIP lookup.
			if (this.geoip != null)
				this.geoip.close();

			// Send quit.
			this.send("QUIT :" + stringTable.getProperty("quitMessage", "Ping timeout: 9001 seconds"));

			// Close things.
			if (netOut != null)
				netOut.close();
			if (netIn != null)
				netIn.close();
			if (socket != null)
				socket.close();

			this.stdPrintln("IRC connection closed.");

			this.cleanedUp = true;
		}
	}

	// ===========================================

	public String getString(String name) {
		return this.stringTable.getProperty(name);
	}

	public String getString(String name, Object... args) {
		return String.format(this.stringTable.getProperty(name), args);
	}

	public void sendMemo(String nick, String memo) {
		synchronized (memoQueue) {
			memoQueue.add(new MemoQueueItem(nick, memo));
		}
	}

	private void sendMemo(MemoQueueItem memo) {
		this.privmsg("MemoServ", "SEND " + memo.destination + " " + memo.message);
	}

	// ===========================================

	public synchronized void stdPrintln(String s) {
		DateFormat df = new SimpleDateFormat(this.config.getProperty("timestampFormat", ""));
		String date = df.format(new Date());

		System.out.println(date + "[ MAIN ] " + s);
	}

	public synchronized void stdPrintln(String format, Object... args) {
		stdPrintln(String.format(format, args));
	}

	public synchronized void errPrintln(String s) {
		DateFormat df = new SimpleDateFormat(this.config.getProperty("timestampFormat", ""));
		String date = df.format(new Date());

		System.err.println(date + "[ MAIN ] " + s);
	}

	public synchronized void errPrintln(String format, Object... args) {
		errPrintln(String.format(format, args));
	}

	// ===========================================

	public String getAdminChannel() {
		return this.config.getProperty("adminChannel");
	}

	public boolean getUserServerConnected(UserDB.UserEntry ue) {
		return this.bncManager.getUserServer(ue).isConnected();
	}

	// ===========================================

	public void loadConfig() throws FileNotFoundException, IOException {
		if (this.configFile == null)
			return;

		stdPrintln("Reading configuration file: " + this.configFile.getName());

		FileInputStream fis = new FileInputStream(this.configFile);
		this.config.load(fis);
		fis.close();
	}

	public void loadStringTable() throws FileNotFoundException, IOException {
		if (this.stringTableFile == null)
			return;

		stdPrintln("Reading string table: " + this.stringTableFile.getName());

		FileInputStream fis = new FileInputStream(this.stringTableFile);
		this.stringTable.load(fis);
		fis.close();
	}

	// ===========================================

	public String lookupNick(String nick) {
		try {
			synchronized (this.whyLookup) {
				this.privmsg("ChanServ", "ACCESS " + this.config.getProperty("whyChannel") + " ADD " + nick + " 3");
				this.privmsg("ChanServ", "WHY " + this.config.getProperty("whyChannel") + " " + nick);
				this.privmsg("ChanServ", "ACCESS " + this.config.getProperty("whyChannel") + " DEL " + nick);

				long time = System.currentTimeMillis();
				while (!this.whyLookup.containsKey(nick) && ((time + 15000) > System.currentTimeMillis())) { // While we don't have a result. Time out after 15 seconds.
					this.whyLookup.wait(1000);
				}

				return this.whyLookup.remove(nick);
			}
		} catch (Exception e) {
			return null;
		}
	}

	public boolean lookupIdentified(String nick) {
		try {
			synchronized (this.identifiedLookup) {
				this.send("WHOIS " + nick);

				long time = System.currentTimeMillis();
				while (!this.identifiedLookup.containsKey(nick) && ((time + 10000) > System.currentTimeMillis())) { // While we don't have a result. Time out after 10 seconds.
					this.identifiedLookup.wait(1000);
				}

				Boolean result = this.identifiedLookup.remove(nick);
				if (result == null)
					return false;
				else
					return result;
			}
		} catch (Exception e) {
			return false;
		}
	}

	public boolean lookupOper(String nick) {
		try {
			synchronized (this.operLookup) {
				this.send("WHO " + nick);

				long time = System.currentTimeMillis();
				while (!this.operLookup.containsKey(nick.toLowerCase()) && ((time + 5000) > System.currentTimeMillis())) { // While we don't have a result. Time out after 5 seconds.
					this.operLookup.wait(1000);
				}

				Boolean result = this.operLookup.remove(nick.toLowerCase());
				if (result == null)
					return false;
				else
					return result;
			}
		} catch (Exception e) {
			return false;
		}
	}

	public Date lookupRegTime(String nick) {
		try {
			synchronized (this.regTimeLookup) {
				this.privmsg("NickServ", "INFO " + nick);

				long time = System.currentTimeMillis();
				while (!this.regTimeLookup.containsKey(nick) && ((time + 15000) > System.currentTimeMillis())) { // While we don't have a result. Time out after 15 seconds.
					this.regTimeLookup.wait(1000);
				}

				return this.regTimeLookup.remove(nick);
			}
		} catch (Exception e) {
			return null;
		}
	}

	// ===========================================

	public void privmsg(String target, String msg) {
		if (msg.length() > 400) {
			for (int i = 400; i > 0; i--) {
				if (msg.substring(i, i + 1).matches("\\s")) {
					doPrivmsg(target, msg.substring(0, i));
					privmsg(target, msg.substring(i));
					return;
				}
			}
		} else {
			doPrivmsg(target, msg);
		}
	}

	public void notice(String target, String msg) {
		if (msg.length() > 400) {
			for (int i = 400; i > 0; i--) {
				if (msg.substring(i, i + 1).matches("\\s")) {
					doNotice(target, msg.substring(0, i));
					notice(target, msg.substring(i));
					return;
				}
			}
		} else {
			doNotice(target, msg);
		}
	}

	private void doPrivmsg(String target, String line) {
		this.send("PRIVMSG " + target + " :" + line);
	}

	private void doNotice(String target, String line) {
		this.send("NOTICE " + target + " :" + line);
	}

	public void send(String line) {
		formatOut(line);
		netOut.println(line);
	}

	private void formatOut(String s) {
		if (this.verbose)
			this.stdPrintln("-> " + s);
	}

	private void formatIn(String s) {
		if (this.verbose)
			this.stdPrintln("<- " + s);
	}

	// ===========================================

	private void getGeoIP() {
		File f = new File("GeoIP.dat");
		if (f.exists())
			return;

		try {
			// Download the latest. >.>
			this.stdPrintln("GeoIP Database not found; downloading latest.");
			URL u = new URL("http://geolite.maxmind.com/download/geoip/database/GeoLiteCountry/GeoIP.dat.gz");
			InputStream is = u.openStream();
			is = new GZIPInputStream(new BufferedInputStream(is));
			OutputStream os = new FileOutputStream(f);

			byte[] b = new byte[1024];
			int len;
			while ((len = is.read(b)) > 0) {
				os.write(b, 0, len);
			}

			is.close();
			os.close();
			this.stdPrintln("Download complete, continuing.");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// ===========================================

	private class PingTimer extends TimerTask {
		@Override
		public void run() {
			if (serverPing.size() > 0) {
				errPrintln("IRC server ping timeout. Attempting reconnect.");

				try {
					netIn.close();
					netInStream.close();
				} catch (Exception e) {
				}
			}

			String pingId = "" + System.currentTimeMillis() / 1000;

			serverPing.add(pingId);
			send("PING " + pingId);
		}
	}

	private class DatabaseDaemon extends TimerTask {
		@Override
		public void run() {
			try {
				database.saveData();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private class UnsuspendTimer extends TimerTask {
		@Override
		public void run() {
			try {
				Connection conn = DatabaseConnection.getConnection();
				PreparedStatement ps = conn.prepareStatement("SELECT * FROM `bncbot_suspension` WHERE `suspend_expiry` < CURRENT_TIMESTAMP;");
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					UserEntry ue = database.findUser(rs.getInt("user_id"));
					if (ue == null)
						stdPrintln("Couldn't find user (ID: " + rs.getInt("user_id") + ") for unsuspension!");

					privmsg(getAdminChannel(), getString("adminExpire", ue.getNick(), ue.getId(), ue.getActionReason()));
					database.unsuspendUser(ue, "Automatic!Unsuspension@Timer");
					bncManager.getUserServer(ue).unblockUser(ue.getNick());
				}

				ps.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private class MemoSendTimer extends TimerTask {
		@Override
		public void run() {
			try {
				synchronized (memoQueue) {
					MemoQueueItem m = memoQueue.poll();

					if (m != null)
						sendMemo(m);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private static class MemoQueueItem {
		public String destination, message;
		public MemoQueueItem(String destination, String message) {
			this.destination = destination;
			this.message = message;
		}
	}
}

class MessageProc extends Thread {
	private String line, words[];

	public MessageProc(String line) {
		super(null, null, "Message Command");

		this.words = line.split("\\s+");
		this.line = line;
	}

	@Override
	public void run() {
		if (this.words.length < 4) {
			// Garbage, how did it even get past to here?
			return;
		}

		CommandManager hm = CommandManager.getInstance();
		String command = words[3].substring(1);

		Command h = hm.getHandler(command);
		if ((h != null) && h.prepare(line))
			h.run();
	}
}
