package net.rizon.bncbot;

public interface Command {
	/**
	 * Prepares this command to be run.
	 *
	 * @param line The full IRC message line that caused this command to be triggered.
	 * @return <code>true</code> if the command can be run (i.e. all prerequisites have been met), <code>false</code> if the command should be ignored.
	 */
	public boolean prepare(String line);

	/**
	 * Run the command. {@link Command#prepare(String)} should be called prior, otherwise behavior is undefined.
	 */
	public void run();
}
