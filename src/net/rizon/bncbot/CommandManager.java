package net.rizon.bncbot;

import java.util.Hashtable;

import net.rizon.bncbot.commands.*;

public final class CommandManager {
	private static CommandManager instance = new CommandManager();

	private Hashtable<String, Command> handlers = new Hashtable<String, Command>();

	public final static String versionString = "Command Manager: $Id$";

	// Prevent instantiation.
	private CommandManager() {
	}

	void reset() {
		// Reassign command handlers.
		handlers = new Hashtable<String, Command>();

		// Data admin commands.
		handlers.put(".bncapprove", new BncApproveCmd());
		handlers.put(".ba", handlers.get(".bncapprove"));

		handlers.put(".bncreject", new BncRejectCmd());
		handlers.put(".br", handlers.get(".bncreject"));

		handlers.put(".bncdelete", new BncDeleteCmd());
		handlers.put(".bd", handlers.get(".bncdelete"));

		handlers.put(".bncsuspend", new BncSuspendCmd());
		handlers.put(".bncunsuspend", handlers.get(".bncsuspend"));
		handlers.put(".bs", handlers.get(".bncsuspend"));
		handlers.put(".bus", handlers.get(".bncsuspend"));
		handlers.put(".bnclistsuspend", new BncListSuspendCmd());

		handlers.put(".bncadd", new BncAddCmd());

		handlers.put(".bncmove", new BncMoveCmd());

		handlers.put(".bncbl", new BncBlCmd());

		handlers.put(".bncinfo", new BncInfoCmd());
		handlers.put(".bi", handlers.get(".bncinfo"));
		handlers.put(".bncnote", new BncNoteCmd());
		handlers.put(".bncfind", new BncFindCmd());
		handlers.put(".bncuserstats", new BncStatsCmd());
		handlers.put(".bncsearch", handlers.get(".bncfind"));
		handlers.put(".bnclastseen", new BncLastSeenCmd());
		handlers.put(".bncls", handlers.get(".bnclastseen"));
		handlers.put(".bp", new BncNotProcessedCmd());
		handlers.put(".bncabout", new BncAboutCmd());
		handlers.put(".bncoverride", new BncGenerateOverrideCmd());
		handlers.put(".bncrehash", new BncRehashCmd());
		handlers.put(".bnchelp", new BncHelpCmd());

		handlers.put(".bncloadmod", new BncLoadModCmd());
		handlers.put(".bncunloadmod", handlers.get(".bncloadmod"));

		handlers.put(".loggrep", new LogGrepCmd());

		// Server admin commands.
		handlers.put(".serverlist", new ServerListCmd());
		handlers.put(".serverenable", new ServerEnableCmd());
		handlers.put(".serverdisable", handlers.get(".serverenable"));

		// Misc admin commands.
		handlers.put(".savedata", new SaveDataCmd());
		handlers.put(".forcesavedata", handlers.get(".savedata"));

		// Run-state admin commands.
		handlers.put(".shutdownbot", new ShutdownCmd());

		// User PM commands.
		handlers.put("request", new RequestCmd());
		handlers.put("status", new StatusCmd());
		handlers.put("info", handlers.get("status"));
		handlers.put("help", new HelpCmd());
		handlers.put("changepass", new ChangePassCmd());

		// Admin PM Commands.
		handlers.put("realip", new RealIPCmd());
		handlers.put("searchip", new SearchIPCmd());
		handlers.put("iplog", new BncIPLogCmd());
	}

	public Command getHandler(String command) {
		if (handlers == null)
			throw new IllegalStateException("CommandManager not initialized! (call .reset() before any calls to .getHandler())");

		return handlers.get(command.toLowerCase());
	}

	public static CommandManager getInstance() {
		return instance;
	}
}
