package net.rizon.bncbot;

import java.util.Random;

public final class Helpers {
	private static Random rnd = new Random();

	private Helpers() { // Static class cannot be instantiated.
	}

	public static boolean isValidUsername(String username) {
		char p = username.charAt(0);

		if (username.isEmpty())
			return false;

		if (!Character.isLetter(p))
			return false;

		for (int i = 1; i < username.length(); i++) {
			p = username.charAt(i);

			if (p != '@' && p != '.' && p != '-' && p != '_' && !Character.isLetterOrDigit(p))
				return false;
		}

		return true;
	}

	public static String implode(String[] ary, String delim) {
		String out = "";
		for (int i = 0; i < ary.length; i++) {
			if (i != 0) {
				out += delim;
			}
			out += ary[i];
		}
		return out;
	}

	public static int unixTimeToDaysSince(long time) {
		long timeDiff = System.currentTimeMillis() - time;
		return (int) (timeDiff / 86400000L);
	}

	public static String randomPass() {
		return randomPass(6);
	}

	public static String randomPass(int length) {
		StringBuilder sb = new StringBuilder();

		for (int i = 0; i < length; i++) {
			int upperLower = rnd.nextInt(3);
			switch (upperLower) {
				case 0: // Upper
					sb.append((char) ('A' + rnd.nextInt(26)));
					break;
				case 1: // Lower
					sb.append((char) ('a' + rnd.nextInt(26)));
					break;
				case 2: // Number
				default: // WTF.
					sb.append((char) ('0' + rnd.nextInt(10)));
					break;
			}
		}

		return sb.toString();
	}

	public static boolean byteArrayEquals(byte[] val1, byte[] val2) {
		if (val1.length != val2.length)
			return false;

		for (int i = 0; i < val1.length; i++)
			if (val1[i] != val2[i])
				return false;

		return true;
	}

	public static boolean charArrayEquals(char[] val1, char[] val2) {
		if (val1.length != val2.length)
			return false;

		for (int i = 0; i < val1.length; i++)
			if (val1[i] != val2[i])
				return false;

		return true;
	}

	public static String htmlEntities(String s) {
		StringBuilder b = new StringBuilder(s.length());

		for (int i = 0; i < s.length(); i++) {
			char ch = s.charAt(i);
			if (ch >= 'a' && ch <= 'z' || ch >= 'A' && ch <= 'Z' || ch >= '0' && ch <= '9') {
				b.append(ch);
			} else if (Character.isWhitespace(ch)) {
				b.append(ch);
			} else if (Character.isISOControl(ch)) {
			} else if (Character.isHighSurrogate(ch)) {
				int codePoint;
				if (i + 1 < s.length() && Character.isSurrogatePair(ch, s.charAt(i + 1)) && Character.isDefined(codePoint = (Character.toCodePoint(ch, s.charAt(i + 1))))) {
					b.append("&#").append(codePoint).append(";");
				}
				i++;
			} else if (Character.isLowSurrogate(ch)) {
				i++;
			} else {
				if (Character.isDefined(ch)) {
					b.append("&#").append((int) ch).append(";");
				}
			}
		}

		return b.toString();
	}

	public static boolean matchesWithWildcards(String text, String pattern) {
		String[] cards = pattern.split("\\*");

		// Pre-check, if the first card doesn't match the beginning of the text
		// and the pattern doesn't start with a wildcard, return false.
		if(!pattern.startsWith("*") && !text.startsWith(cards[0]))
			return false;
		
		// Pre-check, if the last card doesn't match the end of the text and
		// the pattern doesn't end with a wildcard, return false.
		if(!pattern.endsWith("*") && !text.endsWith(cards[cards.length - 1]))
			return false;

		for(int i = 0; i < cards.length; i++) {
			// Gets the index of the wildcard "word".
			int idx = text.indexOf(cards[i]);

			// If the card wasn't found, return false.
			if(idx == -1)
				return false;

			// Advance the text to the character just after the end of the
			// match.
			text = text.substring(idx + cards[i].length());
		}

		return true;
	}

}
