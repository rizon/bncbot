package net.rizon.bncbot.commands;

import net.rizon.bncbot.AbstractSimpleCommand;
import net.rizon.bncbot.UserDB;

public class BncRejectCmd extends AbstractSimpleCommand {
	@Override
	public boolean prepare(String line) {
		super.prepare(line);
		
		// We want the message to look like this:
		// :nick!ident@host PRIVMSG #adminchan :command arg reason+++++
		// 0                1       2          3        4   5..6..7....
		return args[2].equalsIgnoreCase(bot.getAdminChannel()) && args.length >= 5;
	}
	
	@Override
	public void run() {
		if(args.length < 6) {
			bot.privmsg(bot.getAdminChannel(), bot.getString("adminErrorReasonRequired"));
			return;
		}

		String value = args[4];
		String reason = argsv[5];

		// Try as a nick.
		UserDB.UserEntry ue = bot.database.findUser(value);
		if (ue == null) {
			// Try as a integer.
			try {
				int intValue = Integer.parseInt(value);
				ue = bot.database.findUser(intValue);
			} catch (NumberFormatException e) {
				ue = null; // Oh well. :/
			}
		}
		if (ue != null) {
			// We're good.
			if (ue.getState() == UserDB.UserEntry.UserState.NotProcessed) { // Can only reject non-approved things.
				bot.database.rejectUser(ue, args[0].substring(1), reason);

				bot.privmsg(bot.getAdminChannel(), bot.getString("adminBncAction", bot.getString("adminRejected")));
				bot.sendMemo(ue.getNick(), bot.getString("rejectionMemo", reason, bot.getString("userAppeal")));
			} else {
				bot.privmsg(bot.getAdminChannel(), bot.getString("adminAlreadyProcessed"));
			}
		} else {
			bot.privmsg(bot.getAdminChannel(), bot.getString("adminNoMatch"));
		}
	}

}
