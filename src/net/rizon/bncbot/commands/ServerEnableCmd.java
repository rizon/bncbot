package net.rizon.bncbot.commands;

import net.rizon.bncbot.AbstractSimpleCommand;
import net.rizon.bncmanager.BncServer;

public class ServerEnableCmd extends AbstractSimpleCommand {
	@Override
	public boolean prepare(String line) {
		super.prepare(line);

		return args[2].equalsIgnoreCase(bot.getAdminChannel()) && args.length == 5;
	}

	@Override
	public void run() {
		String nick = args[0].substring(1).split("!")[0];
		
		BncServer s = bot.bncManager.getServer(args[4]);
		if(s != null) {
			if(args[3].equals(":.serverenable"))
				s.enable();
			else
				s.disable();
			
			bot.notice(nick, s.toString() + " - Enabled: " + s.isEnabled());
		} else {
			bot.privmsg(nick, bot.getString("adminNoMatch"));
		}
	}
}
