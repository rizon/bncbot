package net.rizon.bncbot.commands;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;

import net.rizon.DatabaseConnection;
import net.rizon.bncbot.AbstractSimpleCommand;
import net.rizon.bncbot.UserDB.UserEntry;

public class BncListSuspendCmd extends AbstractSimpleCommand {
	@Override
	public boolean prepare(String line) {
		super.prepare(line);

		return args[2].equalsIgnoreCase(bot.getAdminChannel());
	}

	@Override
	public void run() {
		String nick = args[0].substring(1).split("!")[0];

		try {
			Connection conn = DatabaseConnection.getConnection();
			PreparedStatement ps = conn.prepareStatement("SELECT * FROM `bncbot_suspension`;");
			ResultSet rs = ps.executeQuery();

			bot.notice(nick, bot.getString("suspendHeader"));
			int i = 0;
			while (rs.next()) {
				UserEntry ue = bot.database.findUser(rs.getInt("user_id"));
				long secLeft = (rs.getTimestamp("suspend_expiry").getTime() - new Date().getTime()) / 1000;
				if (secLeft < 0)
					continue; // Un-expired entry waiting for timer.

				bot.notice(nick, bot.getString("suspendEntry", rs.getInt("suspend_id"), ue.getNick(), //
					ue.getActionByNick() + "!" + ue.getActionByHostmask(), ue.getActionTime().toString(), //
					(secLeft / 60) + "m" + (secLeft % 60) + "s"));

				i++;
			}

			ps.close();
			bot.notice(nick, bot.getString("suspendFooter", i));
		} catch (Exception e) {
			e.printStackTrace();
			bot.notice(nick, bot.getString("genericError"));
		}
	}
}
