package net.rizon.bncbot.commands;

import java.util.ArrayList;

import net.rizon.Pair;
import net.rizon.bncbot.AbstractSimpleCommand;
import net.rizon.bncmanager.BncServer;

public class SearchIPCmd extends AbstractSimpleCommand {
	@Override
	public boolean prepare(String line) {
		super.prepare(line);

		// Check for proper number of arguments.
		return args[2].equalsIgnoreCase(bot.config.getProperty("nick")) && args.length == 5;
	}

	@Override
	public void run() {
		// Do some stuff.
		String user = args[0].substring(1);
		String nick = user.split("!")[0];

		// Check to make sure is oper or me!
		boolean ident = bot.lookupOper(nick);
		if (!ident) {
			// Silently ignore the command.
			return;
		}

		// Query ZNC.
		String ipMask = args[4];

		try {
			BncServer[] servers = bot.bncManager.getAllServers();

			for (BncServer s : servers) {
				// Connected check.
				if (!s.isConnected()) {
					bot.notice(nick, bot.getString("errorBncNotConnectedSpecific", s.getServerHostname()));
					continue;
				}
				
				ArrayList<Pair<String, String>> list = s.searchIP(ipMask);
				
				if (list.size() > 0) {
					bot.notice(nick, bot.getString("adminUsersMatching", s.getServerAbbr()));

					for (Pair<String, String> p : list)
						bot.notice(nick, p.first + ": " + p.second);
				}
			}
		} catch (RuntimeException re) {
			bot.notice(nick, bot.getString("genericErrorReason", bot.getString(re.getMessage())));
		}
	}
}
