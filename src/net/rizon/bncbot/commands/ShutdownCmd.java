package net.rizon.bncbot.commands;

import java.io.IOException;

import net.rizon.bncbot.AbstractSimpleCommand;

public class ShutdownCmd extends AbstractSimpleCommand {
	@Override
	public boolean prepare(String line) {
		super.prepare(line);
		
		// Checking for admin channel-age.
		return args[2].equalsIgnoreCase(bot.getAdminChannel());
	}
	
	@Override
	public void run() {
		String user = args[0].substring(1);
		String nick = user.split("!")[0];

		// Check to make sure is oper or me!
		boolean ident = bot.lookupOper(nick);
		if (!ident) {
			// Silently ignore the command.
			return;
		}

		bot.stdPrintln("Shutting down on request from IRC.");

		bot.running = false;
		try {
			bot.cleanUp();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
