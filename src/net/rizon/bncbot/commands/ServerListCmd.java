package net.rizon.bncbot.commands;

import net.rizon.bncbot.AbstractSimpleCommand;
import net.rizon.bncmanager.BncServer;

public class ServerListCmd extends AbstractSimpleCommand {
	@Override
	public boolean prepare(String line) {
		super.prepare(line);

		return args[2].equalsIgnoreCase(bot.getAdminChannel());
	}

	@Override
	public void run() {
		String nick = args[0].substring(1).split("!")[0];
		
		for(BncServer s : bot.bncManager.getAllServers())
			bot.notice(nick, s.toString() + " - Enabled: " + s.isEnabled() + ", Connected: " + s.isConnected());
	}
}
