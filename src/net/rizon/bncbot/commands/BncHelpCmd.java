package net.rizon.bncbot.commands;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import net.rizon.DatabaseConnection;
import net.rizon.bncbot.AbstractSimpleCommand;

public class BncHelpCmd extends AbstractSimpleCommand {
	@Override
	public boolean prepare(String line) {
		super.prepare(line);

		// We want the message to look like this:
		// :nick!ident@host PRIVMSG #adminchan :command
		return args[2].equalsIgnoreCase(bot.getAdminChannel());
	}

	@Override
	public void run() {
		String nick = args[0].substring(1).split("!")[0];

		try {
			Connection conn = DatabaseConnection.getConnection();
			
			PreparedStatement ps = conn.prepareStatement("SELECT * FROM `bncbot_help` ORDER BY `help_category`, `help_command`");
			ResultSet rs = ps.executeQuery();
			
			bot.notice(nick, bot.getString("dotHelpHeader"));
			bot.notice(nick, bot.getString("dotHelpNote1"));
			bot.notice(nick, bot.getString("dotHelpNote2"));
			
			int totalCommands = 0;
			String lastCategory = "";
			while(rs.next()) {
				totalCommands++;
				
				if(!rs.getString("help_category").equals(lastCategory))
					bot.notice(nick, bot.getString("dotHelpCategoryHeader", rs.getString("help_category")));
				
				bot.notice(nick, bot.getString("dotHelpCommand", rs.getString("help_command"), rs.getString("help_params"), rs.getString("help_description")));
				lastCategory = rs.getString("help_category");
			}
			
			bot.notice(nick, bot.getString("dotHelpMore"));
			bot.notice(nick, bot.getString("dotHelpEnd", totalCommands));
			rs.close();
			ps.close();
		} catch(NullPointerException npe) {
			bot.notice(nick, bot.getString("genericErrorReason", "errorSQLUnavailable"));
			npe.printStackTrace();
		} catch(Exception e) {
			bot.notice(nick, bot.getString("genericErrorReason", e.getMessage()));
			e.printStackTrace();
		}
	}
}
