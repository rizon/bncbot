package net.rizon.bncbot.commands;

import net.rizon.bncbot.AbstractSimpleCommand;

public class BncRehashCmd extends AbstractSimpleCommand {
	@Override
	public boolean prepare(String line) {
		super.prepare(line);

		// We want the message to look like this:
		// :nick!ident@host PRIVMSG #adminchan :command arg
		return args[2].equalsIgnoreCase(bot.getAdminChannel());
	}

	@Override
	public void run() {
		boolean config = true, strings = true, bnc = true, db = true;
		String nick = args[0].split("!")[0].substring(1);

		if (args.length >= 5) {
			config = argsv[4].contains("bncbot");
			strings = argsv[4].contains("strings");
			bnc = argsv[4].contains("bncserver");
			db = argsv[4].contains("db");
		}

		try {
			if (config) {
				bot.notice(nick, "Rehashing config...");
				bot.loadConfig();
			}
			if (strings) {
				bot.notice(nick, String.format("Reloading string table [%s]...", bot.config.getProperty("stringsLanguage")));
				bot.loadStringTable();
			}
			if (bnc) {
				bot.notice(nick, "Reloading BNC information fron SQL...");
				bot.bncManager.cleanUp();
				bot.bncManager.init();
			}
			if(db) {
				bot.notice(nick, "Reloading user database from SQL...");
				bot.database.rehash();
			}
			bot.notice(nick, "done!");
		} catch (Exception e) {
			e.printStackTrace();
			bot.notice(nick, "failed, backtrace printed to console.");
		}
	}
}
