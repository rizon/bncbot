package net.rizon.bncbot.commands;

import java.sql.Connection;
import java.sql.PreparedStatement;

import net.rizon.DatabaseConnection;
import net.rizon.bncbot.AbstractSimpleCommand;
import net.rizon.bncbot.Helpers;

public class BncGenerateOverrideCmd extends AbstractSimpleCommand {
	public static String override = null;

	@Override
	public boolean prepare(String line) {
		super.prepare(line);

		// We want the message to look like this:
		// :nick!ident@host PRIVMSG #adminchan :command arg
		return args[2].equalsIgnoreCase(bot.getAdminChannel()) && args.length >= 4;
	}

	@Override
	public void run() {
		if (override != null) {
			if (args.length == 5 && args[4].equalsIgnoreCase("clear")) {
				Connection conn = DatabaseConnection.getConnection();
				override = null;

				try {
					PreparedStatement ps = conn.prepareStatement("INSERT INTO `bncbot_log` VALUES (NULL, ?, NULL);");

					ps.setString(1, String.format("Admin %s cleared an existing override code.", args[0].substring(1)));
					ps.execute();
					ps.close();
				} catch (Exception e) {
					e.printStackTrace();
				}

				bot.privmsg(bot.getAdminChannel(), bot.getString("adminOverrideCodeCleared"));
			} else {
				bot.privmsg(bot.getAdminChannel(), bot.getString("adminErrorOverrideCodeExists", override));
			}
		} else {
			Connection conn = DatabaseConnection.getConnection();
			override = Helpers.randomPass(10);

			try {
				PreparedStatement ps = conn.prepareStatement("INSERT INTO `bncbot_log` VALUES (NULL, ?, NULL);");

				ps.setString(1, String.format("Admin %s generated an override code.", args[0].substring(1)));
				ps.execute();
				ps.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

			bot.privmsg(bot.getAdminChannel(), bot.getString("adminOverrideCode", override));
		}
	}
}
