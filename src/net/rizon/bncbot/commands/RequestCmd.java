package net.rizon.bncbot.commands;

import java.util.ArrayList;
import java.util.Date;

import net.rizon.bncbot.AbstractSimpleCommand;
import net.rizon.bncbot.Helpers;
import net.rizon.bncbot.UserDB.UserEntry;
import net.rizon.bncbot.UserDB.UserEntry.UserState;
import net.rizon.bncmanager.BncServer;

public class RequestCmd extends AbstractSimpleCommand {
	@Override
	public boolean prepare(String line) {
		super.prepare(line);

		// Check to see that it comes via PM.
		return args[2].equalsIgnoreCase(bot.config.getProperty("nick"));
	}

	@Override
	public void run() {
		// Do some stuff.
		String user = args[0].substring(1);
		String nick = user.split("!")[0];
		String hostmask = user.split("!")[1];

		bot.privmsg(nick, bot.getString("userPleaseWait")); // Yes, this process takes a long time.

		// Check to make sure identified.
		boolean ident = bot.lookupIdentified(nick);
		if(!ident) {
			bot.privmsg(nick, bot.getString("genericErrorReason", bot.getString("userErrorIdentify")));
			System.out.println(user + " no ident");
			return;
		}

		// Look up other stuff. Separate checks to improve response time.
		String mainNick = bot.lookupNick(nick);
		if(mainNick == null) {
			bot.privmsg(nick, bot.getString("genericErrorReason", bot.getString("userErrorInfo")));
			System.out.println(user + " no nick");
			return;
		}
		Date regTime = bot.lookupRegTime(mainNick);
		if(regTime == null) {
			bot.privmsg(nick, bot.getString("genericErrorReason", bot.getString("userErrorInfo")));
			System.out.println(user + " no regtime");
			return;
		}
		
		// Check for username validity.
		if(!Helpers.isValidUsername(mainNick)) {
			bot.privmsg(nick, bot.getString("genericErrorReason", bot.getString("userErrorInvalidUsername")));
			bot.privmsg(bot.getAdminChannel(), bot.getString("adminAutoReject", nick, mainNick, hostmask, bot.getString("arIllegalNick")));
			return;
		}
		
		// Check for blacklisting.
		int blId = bot.database.isBlacklisted(user);
		if(blId != -1) {
			bot.privmsg(nick, bot.getString("userBlacklisted"));
			bot.privmsg(nick, bot.getString("userDecisionInError"));
			bot.privmsg(bot.getAdminChannel(), bot.getString("adminAutoReject", nick, mainNick, hostmask, bot.getString("arBlacklisted", blId)));
			return;
		}
		
		// Calculations.
		int daysDifference = Helpers.unixTimeToDaysSince(regTime.getTime());

		if (bot.debug) { // Extraneous information.
			bot.privmsg(nick, "Your main nick is: " + mainNick);
			bot.privmsg(nick, "You registered on: " + regTime.toString());
			bot.privmsg(nick, "You registered " + daysDifference + " day(s) ago.");
		}

		boolean overrideUsed = args.length >= 5 && args[4].equals(BncGenerateOverrideCmd.override);
		if (daysDifference >= 7 || overrideUsed) {
			// Reset override code if used.
			if(overrideUsed)
				BncGenerateOverrideCmd.override = null;
			
			// Check to make sure this is a new application.
			UserEntry ue = bot.database.findUser(mainNick);
			if (ue != null) {
				if (ue.getState() == UserState.Approved) { // Accepted already.
					bot.privmsg(nick, bot.getString("userAlreadyApproved", ue.getActionByNick(), ue.getActionByHostmask(), ue.getActionTime()));
					bot.privmsg(bot.getAdminChannel(), bot.getString("adminAutoReject", nick, mainNick, hostmask, bot.getString("arAlreadyApproved")));
				} else if (ue.getState() == UserState.Rejected) { // Too bad.
					bot.privmsg(nick, bot.getString("userAlreadyRejected", ue.getActionReason()));
					bot.privmsg(nick, bot.getString("userAppeal"));
					bot.privmsg(bot.getAdminChannel(), bot.getString("adminAutoReject", nick, mainNick, hostmask, bot.getString("arAlreadyRejected")));
				} else if (ue.getState() == UserState.Suspended) {
					bot.privmsg(nick, bot.getString("userAlreadySuspended", ue.getActionReason()));
					bot.privmsg(nick, bot.getString("userDecisionInError"));
					bot.privmsg(bot.getAdminChannel(), bot.getString("adminAutoReject", nick, mainNick, hostmask, bot.getString("arAccountSuspended")));
				} else { // There's only one left. :/
					bot.privmsg(nick, bot.getString("userPending"));
				}
			} else {
				// Check for a server preference, otherwise add to default.
				BncServer s = bot.bncManager.getAllServers()[0];
				if(args.length >= (overrideUsed ? 6 : 5)) {
					s = bot.bncManager.getServer(args[overrideUsed ? 5 : 4]);
					
					if(s == null) {
						bot.privmsg(nick, bot.getString("userErrorInvalidServer", bot.bncManager.getServerAbbrList()));
						return;
					}
				} else {
					for(BncServer serv : bot.bncManager.getAllServers())
						if(serv.getUserCount() < s.getUserCount())
							s = serv;
					bot.privmsg(nick, bot.getString("userDefaultServer", s.getServerAbbr()));
				}
				
				if(!s.isConnected()) {
					bot.privmsg(nick, bot.getString("errorBncNotConnectedSpecific", s.getServerAbbr()));
					return;
				}
				
				// Check with our sugoi heuristics.
				ArrayList<String> warnings = bot.database.applyHeuristics(mainNick, hostmask);
				
				// Try to add.
				int id = bot.database.addUser(mainNick, hostmask, s, overrideUsed);
				if (id < 0) {
					bot.privmsg(nick, bot.getString("genericError"));
				} else {
					// Notify the user.
					bot.privmsg(nick, bot.getString("userSubmitted1", id, s.getServerAbbr()));
					bot.privmsg(nick, bot.getString("userSubmitted2"));

					// Notify the staff.
					bot.privmsg(bot.getAdminChannel(), String.format(bot.getString("adminNew"), nick, mainNick, hostmask, id, s.getServerAbbr()));
					
					// Notify for any warnings.
					for(String w : warnings)
						if(w != null)
							bot.privmsg(bot.getAdminChannel(), bot.getString("adminHeuristicWarn", w));
				}
			}
		} else {
			bot.privmsg(nick, String.format(bot.getString("genericErrorReason"), bot.getString("userErrorTime")));
			bot.privmsg(bot.getAdminChannel(), bot.getString("adminAutoReject", nick, mainNick, hostmask, bot.getString("arTime")));
		}
	}
}
