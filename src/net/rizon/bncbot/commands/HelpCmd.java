package net.rizon.bncbot.commands;

import net.rizon.bncbot.AbstractSimpleCommand;

public class HelpCmd extends AbstractSimpleCommand {
	@Override
	public boolean prepare(String line) {
		super.prepare(line);

		// Check to make sure it's coming via PM.
		return args[2].equalsIgnoreCase(bot.config.getProperty("nick"));
	}

	@Override
	public void run() {
		// Do some stuff.
		String nick = args[0].substring(1).split("!")[0];
		
		// Send mass text spam.
		bot.privmsg(nick, bot.getString("helpHeader"));
		bot.privmsg(nick, bot.getString("helpFAQ"));
		bot.privmsg(nick, bot.getString("helpRequest", bot.config.getProperty("nick")));
		bot.privmsg(nick, bot.getString("helpStatus", bot.config.getProperty("nick")));
		bot.privmsg(nick, bot.getString("helpChangePass", bot.config.getProperty("nick")));
		bot.privmsg(nick, bot.getString("helpAppeal", bot.getString("userAppeal")));
		bot.privmsg(nick, bot.getString("helpServers", bot.bncManager.getServerAbbrList()));
		bot.privmsg(nick, bot.getString("helpHelp", bot.config.getProperty("nick")));
	}
}
