package net.rizon.bncbot.commands;

import net.rizon.bncbot.AbstractSimpleCommand;
import net.rizon.bncbot.Helpers;
import net.rizon.bncbot.UserDB.UserEntry;
import net.rizon.bncmanager.BncServer;

public class BncAddCmd extends AbstractSimpleCommand {

	@Override
	public boolean prepare(String line) {
		super.prepare(line);

		// We want the message to look like this:
		// :nick!ident@host PRIVMSG #adminchan :command user server
		// 0----------------1-------2----------3--------4----5
		return args[2].equalsIgnoreCase(bot.getAdminChannel()) && args.length == 6;
	}

	@Override
	public void run() {
		String username = args[4];
		BncServer server = bot.bncManager.getServer(args[5]);

		if (bot.database.findUser(username) != null) {
			bot.privmsg(bot.getAdminChannel(), bot.getString("adminErrorUserExists"));
			return;
		}
		if(server == null) {
			bot.privmsg(bot.getAdminChannel(), bot.getString("userErrorInvalidServer", bot.bncManager.getServerAbbrList()));
			return;
		}
		
		// Add.
		UserEntry ue = bot.database.findUser(bot.database.addUser(username, args[0].substring(1) + " on behalf of " + username, server, false));
		String pass = Helpers.randomPass();
		
		bot.database.approveUser(ue, args[0].substring(1));
		bot.bncManager.getUserServer(ue).addUser(ue.getNick(), pass);
		bot.privmsg(bot.getAdminChannel(), bot.getString("adminCreate", ue.getId(), ue.getNick(), pass));
		bot.sendMemo(ue.getNick(), bot.getString("creationMemo", bot.bncManager.getUserServer(ue).getServerHostname(), ue.getNick(), pass));
	}
}
