package net.rizon.bncbot.commands;

import net.rizon.bncbot.AbstractSimpleCommand;
import net.rizon.bncmanager.BncServer;

public class BncStatsCmd extends AbstractSimpleCommand {
	@Override
	public boolean prepare(String line) {
		super.prepare(line);

		// We want the message to look like this:
		// :nick!ident@host PRIVMSG #adminchan :command
		return args[2].equalsIgnoreCase(bot.getAdminChannel());
	}

	@Override
	public void run() {
		String nick = args[0].split("!")[0].substring(1);

		int t1 = 0, t2 = 0, t3 = 0;
		for (BncServer s : bot.bncManager.getAllServers()) {
			if (!s.isConnected()) {
				bot.notice(nick, bot.getString("adminErrorStatsUnavailable", s.getServerAbbr(), bot.getString("errorBncNotConnected")));
				continue;
			}

			int[] r = s.getStats();
			if (r == null)
				bot.notice(nick, bot.getString("adminErrorStatsUnavailable", s.getServerAbbr(), "Server did not reply within the timeout period."));
			else {
				bot.notice(nick, bot.getString("adminUserStatsServer", s.getServerAbbr(), r[1], r[0], r[2]));
				t1 += r[0]; t2 += r[1]; t3 += r[2];
			}
		}

		bot.notice(nick, bot.getString("adminUserStatsAll", t2, t1, t3));
	}
}
