package net.rizon.bncbot.commands;

import net.rizon.bncbot.AbstractSimpleCommand;
import net.rizon.bncbot.Helpers;
import net.rizon.bncbot.UserDB;
import net.rizon.bncbot.UserDB.UserEntry.UserState;
import net.rizon.bncmanager.BncServer;

public class BncMoveCmd extends AbstractSimpleCommand {
	@Override
	public boolean prepare(String line) {
		super.prepare(line);

		// We want the message to look like this:
		// :nick!ident@host PRIVMSG #adminchan :command arg newserver
		// 0 				1 		2 		   3		4   5
		return args[2].equalsIgnoreCase(bot.getAdminChannel()) && args.length == 6;
	}

	@Override
	public void run() {
		if (args.length < 6) {
			bot.privmsg(bot.getAdminChannel(), bot.getString("adminErrorReasonRequired"));
			return;
		}

		String value = args[4];
		String newserver = args[5];

		// Try as a nick.
		UserDB.UserEntry ue = bot.database.findUser(value);
		if (ue == null) {
			// Try as a integer.
			try {
				int intValue = Integer.parseInt(value);
				ue = bot.database.findUser(intValue);
			} catch (NumberFormatException e) {
				ue = null; // Oh well. :/
			}
		}

		// Now find the new server.
		BncServer newS = bot.bncManager.getServer(newserver);
		if (ue != null) {
			if (newS != null) {
				// Connected checks.
				if (!bot.getUserServerConnected(ue)) {
					bot.privmsg(bot.getAdminChannel(), bot.getString("errorBncNotConnectedSpecific", ue.getBncServerAbbr()));
					return;
				}
				if (!newS.isConnected()) {
					bot.privmsg(bot.getAdminChannel(), bot.getString("errorBncNotConnectedSpecific", newS.getServerAbbr()));
					return;
				}
				
				// Check to see if they were approved or not; delete if they were.
				if(ue.getState() == UserState.Approved) {
					bot.bncManager.getUserServer(ue).delUser(ue.getNick());
				}
				
				// Then set new server.
				bot.database.moveUser(ue, args[0].substring(1), newS);
				
				// Only add and notify if they were approved.
				if(ue.getState() == UserState.Approved) {
					String pass = Helpers.randomPass();
					newS.addUser(ue.getNick(), pass);
					bot.sendMemo(ue.getNick(), bot.getString("moveMemo", newS.getServerHostname(), ue.getNick(), pass));
				}
				bot.privmsg(bot.getAdminChannel(), bot.getString("adminBncAction", bot.getString("adminMoved")));
			} else {
				bot.privmsg(bot.getAdminChannel(), bot.getString("userErrorInvalidServer", bot.bncManager.getServerAbbrList()));
			}
		} else {
			bot.privmsg(bot.getAdminChannel(), bot.getString("adminNoMatch"));
		}
	}

}
