package net.rizon.bncbot.commands;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map.Entry;

import net.rizon.bncbot.AbstractSimpleCommand;
import net.rizon.bncbot.UserDB.UserEntry;
import net.rizon.bncbot.UserDB.UserEntry.UserState;

public class BncFindCmd extends AbstractSimpleCommand {
	@Override
	public boolean prepare(String line) {
		super.prepare(line);

		// We want the message to look like this:
		// :nick!ident@host PRIVMSG #adminchan :command arg
		return args[2].equalsIgnoreCase(bot.getAdminChannel()) && args.length >= 5;
	}

	@Override
	public void run() {
		String[] commandArgs = argsv[4].split("\\s+");
		Hashtable<String, String> kvArgs = new Hashtable<String, String>(commandArgs.length);
		for (String s : commandArgs) {
			String[] ss = s.split("=", 2);
			if (ss.length != 2) {
				bot.privmsg(bot.getAdminChannel(), "Error while parsing search constraints.");
				return;
			}

			kvArgs.put(s.split("=")[0], s.split("=")[1]);
		}

		ArrayList<UserEntry> matches = new ArrayList<UserEntry>();
		synchronized (bot.database) {
			for (UserEntry ue : bot.database) {
				boolean doesMatch = true;

				// Checks through each supplied search param against the matched ones.
				// Values can currently be these types: boolean, integer specifing gt or lt.
				// Matching rules:
				// -Booleans: Everything is "true" except for "false".
				// -Integer: The first character of the string must be "<" or ">". It is then stripped and then evaluated logically.
				for (Entry<String, String> param : kvArgs.entrySet()) {
					if (param.getKey().equalsIgnoreCase("approved")) {
						if (param.getValue().equalsIgnoreCase("false")) {
							doesMatch &= ue.getState() != UserState.Approved;
						} else {
							doesMatch &= ue.getState() == UserState.Approved;
						}
					} else if (param.getKey().equalsIgnoreCase("rejected")) {
						if (param.getValue().equalsIgnoreCase("false")) {
							doesMatch &= ue.getState() != UserState.Rejected;
						} else {
							doesMatch &= ue.getState() == UserState.Rejected;
						}
					} else if (param.getKey().equalsIgnoreCase("processed")) {
						if (param.getValue().equalsIgnoreCase("false")) {
							doesMatch &= ue.getState() == UserState.NotProcessed;
						} else {
							doesMatch &= ue.getState() != UserState.NotProcessed;
						}
					} else if (param.getKey().equalsIgnoreCase("suspended")) {
						if (param.getValue().equalsIgnoreCase("false")) {
							doesMatch &= ue.getState() != UserState.Suspended;
						} else {
							doesMatch &= ue.getState() == UserState.Suspended;
						}
					}
				}

				if (doesMatch)
					matches.add(ue);
			}
		}

		bot.privmsg(bot.getAdminChannel(), bot.getString("adminQueryMatch", matches.size(), argsv[4]));
		if (matches.size() > 0) {
			StringBuilder sb = new StringBuilder();
			for (UserEntry ue : matches) {
				sb.append(ue.getNick() + " (Id: " + ue.getId() + "), ");
			}
			bot.privmsg(bot.getAdminChannel(), sb.toString().substring(0, sb.length() - 2));
		}
	}
}
