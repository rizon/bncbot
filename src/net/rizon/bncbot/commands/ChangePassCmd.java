package net.rizon.bncbot.commands;

import net.rizon.bncbot.AbstractSimpleCommand;
import net.rizon.bncbot.UserDB;

public class ChangePassCmd extends AbstractSimpleCommand {
	@Override
	public boolean prepare(String line) {
		super.prepare(line);

		// 5 space-delimited arguments: ":user PRIVMSG us :changepass newpass"
		return args[2].equalsIgnoreCase(bot.config.getProperty("nick")) && args.length == 5;
	}

	@Override
	public void run() {
		// Do some stuff.
		String user = args[0].substring(1);
		String nick = user.split("!")[0];

		bot.privmsg(nick, bot.getString("userPleaseWait")); // Yes, this process takes a long time.

		// Check to make sure identified.
		boolean ident = bot.lookupIdentified(nick);
		if (!ident) {
			bot.privmsg(nick, bot.getString("genericErrorReason", bot.getString("userErrorIdentify")));
			return;
		}

		// Look up other stuff.
		String mainNick = bot.lookupNick(nick);
		if (mainNick == null) {
			bot.privmsg(nick, bot.getString("genericErrorReason", bot.getString("userErrorInfo")));
			return;
		}

		UserDB.UserEntry ue = bot.database.findUser(mainNick);
		if (ue != null) {
			// Connected check.
			if(!bot.getUserServerConnected(ue)) {
				bot.privmsg(nick, bot.getString("errorBncNotConnected"));
				return;
			}
			
			if (ue.getState() == UserDB.UserEntry.UserState.Approved) { // Go ahead!
				bot.bncManager.getUserServer(ue).changePass(ue.getNick(), args[4]);
				bot.privmsg(nick, bot.getString("userPassChangeSuccess"));
			} else { 
				bot.privmsg(nick, bot.getString("userErrorStatusMismatch"));
			}
		} else {
			bot.privmsg(nick, bot.getString("userStatusNone", bot.config.getProperty("nick")));
		}

	}
}
