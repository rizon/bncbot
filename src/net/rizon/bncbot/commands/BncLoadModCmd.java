package net.rizon.bncbot.commands;

import net.rizon.bncbot.AbstractSimpleCommand;
import net.rizon.bncbot.UserDB;

public class BncLoadModCmd extends AbstractSimpleCommand {
	@Override
	public boolean prepare(String line) {
		super.prepare(line);
		
		// We want the message to look like this:
		// :nick!ident@host PRIVMSG #adminchan :command
		// 0                1       2          3       
		return args[2].equalsIgnoreCase(bot.getAdminChannel()) && args.length >= 6;
	}

	@Override
	public void run() {
		String user = args[4];
		String mod = argsv[5];
		
		// Try as a nick.
		UserDB.UserEntry ue = bot.database.findUser(user);
		if (ue == null) {
			// Try as a integer.
			try {
				int intValue = Integer.parseInt(user);
				ue = bot.database.findUser(intValue);
			} catch (NumberFormatException e) {
				ue = null; // Oh well. :/
			}
		}
		if (ue != null) {
			if (!bot.getUserServerConnected(ue)) {
				bot.privmsg(bot.getAdminChannel(), bot.getString("errorBncNotConnectedSpecific", ue.getBncServerAbbr()));
				return;
			}
			
			String response;
			if(args[3].substring(1).equalsIgnoreCase("bncunload"))
				response = bot.bncManager.getUserServer(ue).unloadModule(ue.getNick(), mod);
			else
				response = bot.bncManager.getUserServer(ue).loadModule(ue.getNick(), mod);
			
			bot.privmsg(bot.getAdminChannel(), bot.getString("adminAdminModReturned", response));
		} else {
			bot.privmsg(bot.getAdminChannel(), bot.getString("adminNoMatch"));
		}
	}
}
