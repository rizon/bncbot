package net.rizon.bncbot.commands;

import net.rizon.bncbot.AbstractSimpleCommand;
import net.rizon.bncbot.UserDB;

public class BncNoteCmd extends AbstractSimpleCommand {
    @Override
    public boolean prepare(String line) {
        super.prepare(line);

        // We want the message to look like this:
        // :nick!ident@host PRIVMSG #adminchan :command arg reason+++++
        // 0                1       2          3        4   5..6..7....
        return args[2].equalsIgnoreCase(bot.getAdminChannel()) && args.length >= 5;
    }

    @Override
    public void run() {
        String[] values = args[4].split(",");
        String note;

        if (args.length < 6)
            note = null;
        else
            note = argsv[5];

        for (String value : values) {
            // Try as a nick.
            UserDB.UserEntry ue = bot.database.findUser(value);
            if (ue == null) {
                // Try as a integer.
                try {
                    int intValue = Integer.parseInt(value);
                    ue = bot.database.findUser(intValue);
                } catch (NumberFormatException e) {
                    ue = null; // Oh well. :/
                }
            }
            if (ue != null) {
                bot.database.setNote(ue, args[0].substring(1), note);
                if (note != null)
                    bot.privmsg(bot.getAdminChannel(), bot.getString("adminNoteSet"));
                else
                    bot.privmsg(bot.getAdminChannel(), bot.getString("adminNoteDel"));
            } else {
                bot.privmsg(bot.getAdminChannel(), bot.getString("adminNoMatch"));
            }
        }
    }

}
