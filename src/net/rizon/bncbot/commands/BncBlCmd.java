package net.rizon.bncbot.commands;

import java.util.Map;

import net.rizon.bncbot.AbstractSimpleCommand;

public class BncBlCmd extends AbstractSimpleCommand {
	public static String override = null;

	@Override
	public boolean prepare(String line) {
		super.prepare(line);

		// We want the message to look like this:
		// :nick!ident@host PRIVMSG #adminchan :command subcommand [arg ...]
		return args[2].equalsIgnoreCase(bot.getAdminChannel()) && args.length >= 5;
	}

	@Override
	public void run() {
		String subcommand = args[4].toLowerCase();
		if (subcommand.equals("add")) {
			try {
				boolean result = bot.database.addBlacklist(args[5]);

				if (result)
					bot.privmsg(bot.getAdminChannel(), bot.getString("adminBlacklistSuccess", bot.getString("adminAdded")));
				else
					bot.privmsg(bot.getAdminChannel(), bot.getString("genericError"));
			} catch (Exception e) {
				bot.privmsg(bot.getAdminChannel(), bot.getString("genericErrorReason", e));
				e.printStackTrace();
			}
		}

		else if (subcommand.equals("del")) {
			try {
				boolean result = bot.database.remBlacklist(Integer.parseInt(args[5]));

				if (result)
					bot.privmsg(bot.getAdminChannel(), bot.getString("adminBlacklistSuccess", bot.getString("adminRemoved")));
				else
					bot.privmsg(bot.getAdminChannel(), bot.getString("genericError"));
			} catch (Exception e) {
				bot.privmsg(bot.getAdminChannel(), bot.getString("genericErrorReason", e));
				e.printStackTrace();
			}
		}

		else if (subcommand.equals("show") || subcommand.equals("list")) {
			Map<Integer, String> table = bot.database.getBlacklist();
			String nick = args[0].split("!")[0].substring(1);

			if (args.length >= 6) {
				try {
					Integer entryNum = Integer.parseInt(args[5]);
					String s = table.get(entryNum);

					bot.notice(nick, bot.getString("blacklistHeader"));
					if (s != null)
						bot.notice(nick, bot.getString("blacklistEntry", entryNum, s));
					bot.notice(nick, bot.getString("blacklistFooter", (s == null) ? 0 : 1));
				} catch (Exception e) {
					bot.notice(nick, bot.getString("blacklistHeader"));
					bot.notice(nick, bot.getString("blacklistFooter", 0));
				}
			} else {
				bot.notice(nick, bot.getString("blacklistHeader"));
				for (Integer i : table.keySet())
					bot.notice(nick, bot.getString("blacklistEntry", i, table.get(i)));
				bot.notice(nick, bot.getString("blacklistFooter", table.size()));
			}
		}
	}
}
