package net.rizon.bncbot.commands;

import net.rizon.bncbot.AbstractSimpleCommand;
import net.rizon.bncbot.UserDB;
import net.rizon.bncbot.UserDB.UserEntry.UserState;

public class StatusCmd extends AbstractSimpleCommand {
	@Override
	public boolean prepare(String line) {
		super.prepare(line);

		// Check to make sure it's coming via PM.
		return args[2].equalsIgnoreCase(bot.config.getProperty("nick"));
	}
	
	@Override
	public void run() {
		// Do some stuff.
		String user = args[0].substring(1);
		String nick = user.split("!")[0];

		bot.privmsg(nick, bot.getString("userPleaseWait")); // Yes, this process takes a long time.
		
		// Check to make sure identified.
		boolean ident = bot.lookupIdentified(nick);
		if(!ident) {
			bot.privmsg(nick, bot.getString("genericErrorReason", bot.getString("userErrorIdentify")));
			return;
		}
		
		// Look up other stuff.
		String mainNick = bot.lookupNick(nick);
		if(mainNick == null) {
			bot.privmsg(nick, bot.getString("genericErrorReason", bot.getString("userErrorInfo")));
			return;
		}
		
		if (bot.debug) { // Extraneous information.
			bot.privmsg(nick, "Your main nick is: " + mainNick);
		}

		UserDB.UserEntry ue = bot.database.findUser(mainNick);
		
		if(ue != null) {
			if (ue.getState() == UserState.Approved) { // Accepted already.
				bot.privmsg(nick, bot.getString("userStatus1", bot.getString("userStatusApproved")));
				bot.privmsg(nick, bot.getString("userStatus2", bot.getString("adminApproved"), ue.getActionByNick(), ue.getActionByHostmask(), ue.getActionTime()));
			} else if (ue.getState() == UserState.Rejected) { // Too bad.
				bot.privmsg(nick, bot.getString("userStatus1", bot.getString("userStatusRejected")));
				bot.privmsg(nick, bot.getString("userStatus2", bot.getString("adminRejected"), ue.getActionByNick(), ue.getActionByHostmask(), ue.getActionTime()));
				bot.privmsg(nick, bot.getString("userStatus3", ue.getActionReason()) + " " + bot.getString("userAppeal"));
			} else if (ue.getState() == UserState.Suspended) { // Suspended.
				bot.privmsg(nick, bot.getString("userStatus1", bot.getString("userStatusSuspended")));
				bot.privmsg(nick, bot.getString("userStatus2", bot.getString("adminSuspended"), ue.getActionByNick(), ue.getActionByHostmask(), ue.getActionTime()));
				bot.privmsg(nick, bot.getString("userStatus3", ue.getActionReason()) + " " + bot.getString("userDecisionInError"));
			} else {
				bot.privmsg(nick, bot.getString("userStatus1", bot.getString("userStatusPending")));
			}
		}
		else {
			bot.privmsg(nick, bot.getString("userStatusNone", bot.config.getProperty("nick")));
		}
	}

}
