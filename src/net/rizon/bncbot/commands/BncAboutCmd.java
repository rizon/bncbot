package net.rizon.bncbot.commands;

import net.rizon.bncbot.AbstractSimpleCommand;
import net.rizon.bncbot.BncBot;
import net.rizon.bncbot.CommandManager;
import net.rizon.bncmanager.BncManager;
import net.rizon.bncmanager.BncServer;

public class BncAboutCmd extends AbstractSimpleCommand {
	@Override
	public boolean prepare(String line) {
		super.prepare(line);

		// We want the message to look like this:
		// :nick!ident@host PRIVMSG #adminchan :command
		// 0                1       2          3
		return args[2].equalsIgnoreCase(bot.getAdminChannel());
	}

	@Override
	public void run() {
		String nick = args[0].split("!")[0].substring(1);

		bot.notice(nick, bot.getString("about", BncBot.class.getPackage().getImplementationVersion()));
		bot.notice(nick, BncBot.versionString);
		bot.notice(nick, BncManager.versionString);
		bot.notice(nick, BncServer.versionString);
		bot.notice(nick, CommandManager.versionString);
	}
}
