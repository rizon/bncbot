package net.rizon.bncbot.commands;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.regex.Pattern;

import net.rizon.DatabaseConnection;
import net.rizon.bncbot.AbstractSimpleCommand;

public class LogGrepCmd extends AbstractSimpleCommand {
	private static final Pattern ip4Address = Pattern.compile("[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}");
	
	@Override
	public boolean prepare(String line) {
		super.prepare(line);

		return args[2].equalsIgnoreCase(bot.getAdminChannel()) && args.length >= 5;
	}

	@Override
	public void run() {
		// Do some stuff.
		String user = args[0].substring(1);
		String nick = user.split("!")[0];
		boolean isOper = bot.lookupOper(nick);

		// Check if we have a starting number.
		int start = -1, num = 10;
		boolean haveStart = true;
		try {
			String[] s = args[4].split(",");
			start = Integer.parseInt(s[0]);
			if(s.length > 1) num = Integer.parseInt(s[1]);
		} catch (NumberFormatException nfe) {
			haveStart = false;
		} finally {
			if (start < 0)
				start = 0;
			if (num < 0)
				num = 10;
		}

		try {
			Connection conn = DatabaseConnection.getConnection();
			String q = "SELECT %s FROM `bncbot_log` WHERE MATCH(`event_text`) AGAINST (? IN BOOLEAN MODE) ORDER BY `event_time` DESC";
			PreparedStatement ps = conn.prepareStatement(String.format(q, "COUNT(*)"));
			ps.setString(1, haveStart ? argsv[5] : argsv[4]);
			ResultSet rs = ps.executeQuery(); rs.next();
			int total = rs.getInt(1);
			rs.close(); ps.close();

			ps = conn.prepareStatement(String.format(q, "`event_text`, `event_time`") + " LIMIT " + start + "," + num);
			ps.setString(1, haveStart ? argsv[5] : argsv[4]);
			rs = ps.executeQuery();

			bot.notice(nick, bot.getString("adminLogGrepHeader", start + 1, Math.min(start + num, total), total, haveStart ? argsv[5] : argsv[4]));
			while (rs.next()) {
				String s = bot.getString("adminLogGrepEntry", new Date(rs.getTimestamp("event_time").getTime()), rs.getString("event_text"));
				if(!isOper) // IPs shouldn't be revealed to non-opers, so replace them with a generic string.
					s = ip4Address.matcher(s).replaceAll("xxx.xxx.xxx.xxx");
				bot.notice(nick, s);
			}
			rs.close(); ps.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
