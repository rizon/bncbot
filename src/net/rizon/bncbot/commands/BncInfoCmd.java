package net.rizon.bncbot.commands;

import java.util.ArrayList;

import net.rizon.bncbot.AbstractSimpleCommand;
import net.rizon.bncbot.Helpers;
import net.rizon.bncbot.UserDB;
import net.rizon.bncbot.UserDB.UserEntry.UserState;
import net.rizon.bncbot.UserDB.UserLogEntry;

public class BncInfoCmd extends AbstractSimpleCommand {
	@Override
	public boolean prepare(String line) {
		super.prepare(line);

		// We want the message to look like this:
		// :nick!ident@host PRIVMSG #adminchan :command arg
		// 0 1 2 3 4
		return args[2].equalsIgnoreCase(bot.getAdminChannel()) && args.length == 5;
	}

	@Override
	public void run() {
		String value = args[4];

		// Try as a nick.
		UserDB.UserEntry ue = bot.database.findUser(value);
		if (ue == null) {
			// Try as a integer.
			try {
				int intValue = Integer.parseInt(value);
				ue = bot.database.findUser(intValue);
			} catch (NumberFormatException e) {
				ue = null; // Oh well. :/
			}
		}
		if (ue != null) {
			boolean processed = ue.getState() != UserState.NotProcessed;
			String stateString;
			if (ue.getState() == UserState.Approved) {
				stateString = bot.getString("adminInfo4Approved");
			} else if (ue.getState() == UserState.Rejected) {
				stateString = bot.getString("adminInfo4Rejected", ue.getActionReason());
			} else if (ue.getState() == UserState.Suspended) {
				stateString = bot.getString("adminInfo4Suspended", ue.getActionReason());
			} else {
				stateString = bot.getString("adminInfo4Unprocessed");
			}

			UserLogEntry connEvent = bot.database.getLastClientEvent(ue);

			bot.privmsg(bot.getAdminChannel(), bot.getString("adminInfo1", ue.getId(), ue.getBncServerAbbr()));
			bot.privmsg(bot.getAdminChannel(), bot.getString("adminInfo2", ue.getNick()));
			bot.privmsg(bot.getAdminChannel(), bot.getString("adminInfo3", ue.getTimeRequested(), ue.getHostmask()));
			bot.privmsg(bot.getAdminChannel(), bot.getString("adminInfo4", stateString));
			if (processed)
				bot.privmsg(bot.getAdminChannel(), bot.getString("adminInfo5", stateString, ue.getActionByNick(), ue.getActionByHostmask(), ue.getActionTime()));
			else
				bot.privmsg(bot.getAdminChannel(), bot.getString("adminInfo5None"));

			if (!processed) {
				ArrayList<String> warnings = bot.database.applyHeuristics(ue.getNick(), ue.getHostmask(), ue.getId());
				for(String w : warnings)
					if(w != null)
						bot.privmsg(bot.getAdminChannel(), bot.getString("adminHeuristicWarn", w));
			} else if (connEvent == null) {
				bot.privmsg(bot.getAdminChannel(), bot.getString("adminInfo6Never"));
			} else if (connEvent.getEntryType().equals("attach")) {
				bot.privmsg(bot.getAdminChannel(), bot.getString("adminInfo6Active"));
			} else {
				bot.privmsg(bot.getAdminChannel(), bot.getString("adminInfo6Detached", connEvent.getEntryTime(),
					Helpers.unixTimeToDaysSince(connEvent.getEntryTime().getTime()), connEvent.wasLastClient() ? " (all clients gone)" : ""));
			}
			if (ue.getNote() != null)
				bot.privmsg(bot.getAdminChannel(), bot.getString("adminInfo7", ue.getNote()));
		} else {
			bot.privmsg(bot.getAdminChannel(), bot.getString("adminNoMatch"));
		}
	}
}
