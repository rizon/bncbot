package net.rizon.bncbot.commands;

import net.rizon.bncbot.AbstractSimpleCommand;
import net.rizon.bncbot.Helpers;
import net.rizon.bncbot.UserDB.UserEntry;
import net.rizon.bncbot.UserDB.UserEntry.UserState;

public class BncApproveCmd extends AbstractSimpleCommand {

	@Override
	public boolean prepare(String line) {
		super.prepare(line);

		// We want the message to look like this:
		// :nick!ident@host PRIVMSG #adminchan :command arg
		// 0----------------1-------2----------3--------4
		return args[2].equalsIgnoreCase(bot.getAdminChannel()) && args.length >= 5;
	}

	@Override
	public void run() {
		String[] values = argsv[4].split("\\s+");

		for (String value : values) {
			// Try as a nick.
			UserEntry ue = bot.database.findUser(value);
			if (ue == null) {
				// Try as a integer.
				try {
					int intValue = Integer.parseInt(value);
					ue = bot.database.findUser(intValue);
				} catch (NumberFormatException e) {
					ue = null; // Oh well. :/
				}
			}

			if (ue != null) {
				// Connected check.
				if (!bot.getUserServerConnected(ue)) {
					bot.privmsg(bot.getAdminChannel(), bot.getString("errorBncNotConnected"));
					return;
				}
				
				// We're good.
				if (ue.getState() != UserState.Approved && ue.getState() != UserState.Suspended) { // Don't want to re-approve, now do we.
					bot.database.approveUser(ue, args[0].substring(1));
					String pass = Helpers.randomPass();

					bot.bncManager.getUserServer(ue).addUser(ue.getNick(), pass);
					bot.privmsg(bot.getAdminChannel(), bot.getString("adminBncActionDetail", bot.getString("adminApproved"), ue.getId()));
					bot.sendMemo(ue.getNick(), bot.getString("approvalMemo", bot.bncManager.getUserServer(ue).getServerHostname(), ue.getNick(), pass));
				} else if (ue.getState() == UserState.Suspended) {
					bot.privmsg(bot.getAdminChannel(), bot.getString("adminUseUnsuspend", ue.getId()));
				} else {
					bot.privmsg(bot.getAdminChannel(), bot.getString("adminAlreadyProcessedDetail", ue.getId()));
				}
			} else {
				bot.privmsg(bot.getAdminChannel(), bot.getString("adminNoMatchDetail", value));
			}
		}
	}
}
