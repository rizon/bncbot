package net.rizon.bncbot.commands;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import net.rizon.DatabaseConnection;
import net.rizon.bncbot.AbstractSimpleCommand;
import net.rizon.bncbot.UserDB;
import net.rizon.bncbot.UserDB.UserLogEntry;

public class BncIPLogCmd extends AbstractSimpleCommand {
	private static final Pattern ip4Address = Pattern.compile("[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}");

	@Override
	public boolean prepare(String line) {
		super.prepare(line);

		// Check for proper number of arguments.
		return args[2].equalsIgnoreCase(bot.config.getProperty("nick")) && args.length == 5;
	}

	@Override
	public void run() {
		// Do some stuff.
		String user = args[0].substring(1);
		String nick = user.split("!")[0];
		String value = args[4];

		// Check to make sure is oper or me!
		boolean ident = bot.lookupOper(nick);
		if (!ident) {
			// Silently ignore the command.
			return;
		}

		// If we're passed an IP address, look up all activity from that IP.
		if (ip4Address.matcher(value).matches())
			try {
				Connection conn = DatabaseConnection.getConnection();
				PreparedStatement ps = conn.prepareStatement("SELECT user_id, event_time, event_type FROM bncbot_conn_log WHERE ip = INET_ATON(?)");
				ps.setString(1, value);
				ResultSet rs = ps.executeQuery();

				if(!rs.next()) {
					bot.notice(nick, bot.getString("adminNoMatch"));
				} else {
					bot.notice(nick, bot.getString("ipLogHeader", value));
					int i = 0;

					do {
						String upperType = Character.toUpperCase(rs.getString("event_type").charAt(0)) + rs.getString("event_type").substring(1);
						bot.notice(nick, bot.getString("ipLog" + upperType + "Entry", new Date(rs.getTimestamp("event_time").getTime()), bot.database.findUser(rs.getInt("user_id"))));
						i++;
					} while (rs.next());

					bot.notice(nick, bot.getString("ipLogFooter", i));
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		else {
			// Try as a nick.
			UserDB.UserEntry ue = bot.database.findUser(value);
			if (ue == null) {
				// Try as a integer.
				try {
					int intValue = Integer.parseInt(value);
					ue = bot.database.findUser(intValue);
				} catch (NumberFormatException e) {
					ue = null; // Oh well. :/
				}
			}
			if (ue != null) {
				List<UserLogEntry> entries = bot.database.getClientLog(ue);

				bot.notice(nick, bot.getString("ipLogHeader", ue.getNick()));
				int i = 0;
				for (UserLogEntry e : entries) {
					String upperType = Character.toUpperCase(e.getEntryType().charAt(0)) + e.getEntryType().substring(1);
					bot.notice(nick, bot.getString("ipLog" + upperType + "Entry", e.getEntryTime(), e.getClientIp()));
					i++;
				}

				bot.notice(nick, bot.getString("ipLogFooter", i));

			} else {
				bot.notice(nick, bot.getString("errorIPLogNoMatch"));
			}
		}
	}
}
