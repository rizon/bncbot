package net.rizon.bncbot.commands;

import java.util.ArrayList;

import net.rizon.bncbot.AbstractSimpleCommand;
import net.rizon.bncbot.UserDB.UserEntry;
import net.rizon.bncbot.UserDB.UserEntry.UserState;

public class BncNotProcessedCmd extends AbstractSimpleCommand {
	@Override
	public boolean prepare(String line) {
		super.prepare(line);

		// We want the message to look like this:
		// :nick!ident@host PRIVMSG #adminchan :command
		// 0                1       2          3
		return args[2].equalsIgnoreCase(bot.getAdminChannel());
	}

	@Override
	public void run() {
		ArrayList<UserEntry> matches = new ArrayList<UserEntry>();

		synchronized (bot.database) {
			for (UserEntry ue : bot.database)
				if (ue.getState() == UserState.NotProcessed)
					matches.add(ue);
		}

		bot.privmsg(bot.getAdminChannel(), bot.getString("adminQueryMatch", matches.size(), "processed=false"));
		if (matches.size() > 0) {
			StringBuilder sb = new StringBuilder();
			for (UserEntry ue : matches) {
				sb.append(ue.getNick() + " (Id: " + ue.getId() + "), ");
			}

			if (sb.length() > 200) {
				bot.privmsg(bot.getAdminChannel(), "Exceeded length limit, response sent via notice.");
				bot.notice(args[0].substring(1).split("!")[0], sb.toString().substring(0, sb.length() - 2));
			} else {
				bot.privmsg(bot.getAdminChannel(), sb.toString().substring(0, sb.length() - 2));
			}
		}
	}
}
