package net.rizon.bncbot.commands;

import net.rizon.bncbot.AbstractSimpleCommand;

public class SaveDataCmd extends AbstractSimpleCommand {
	@Override
	public boolean prepare(String line) {
		super.prepare(line);
		
		// Checking for admin channel-age.
		return args[2].equalsIgnoreCase(bot.getAdminChannel());
	}
	
	@Override
	public void run() {
		bot.database.saveData(args[3].startsWith(":.force"));
		bot.privmsg(args[2], "Data flushed to MySQL.");
	}
}
