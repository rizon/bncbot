package net.rizon.bncbot.commands;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import net.rizon.bncbot.AbstractSimpleCommand;
import net.rizon.bncbot.UserDB.UserEntry;
import net.rizon.bncbot.UserDB.UserLogEntry;

public class BncLastSeenCmd extends AbstractSimpleCommand {
	@Override
	public boolean prepare(String line) {
		super.prepare(line);

		// We want the message to look like this:
		// :nick!ident@host PRIVMSG #adminchan :command arg
		// 0 1 2 3 4
		return args[2].equalsIgnoreCase(bot.getAdminChannel()) && args.length >= 5;
	}

	@Override
	public void run() {
		if (args.length < 5)
			return;

		int days;
		try {
			days = Integer.parseInt(args[4]);
		} catch (NumberFormatException nfe) {
			bot.privmsg(bot.getAdminChannel(), "Argument was not a number.");
			return;
		}

		Map<Integer, UserLogEntry> activity = bot.database.getApprovedUserActivity();
		List<UserEntry> matches = new ArrayList<UserEntry>();

		for (Entry<Integer, UserLogEntry> e : activity.entrySet()) {
			UserEntry ue = bot.database.findUser(e.getKey());

			// Use the larger of last seen time or last approval time.
			// Last seen time is replaced by current time if the last event was a detach,
			// but not all clients were detached.
			// I sincerely apologize to anyone who is reading this.
			long comparisonTime = e.getValue() != null ?
				(e.getValue().getEntryType().equals("detach") ?
					(e.getValue().wasLastClient() ? e.getValue().getEntryTime().getTime() : System.currentTimeMillis()) :
					System.currentTimeMillis()
				) :
				-1;
			comparisonTime = Math.max(comparisonTime, ue.getActionTime().getTime());

			if ((System.currentTimeMillis() - comparisonTime) / 86400000 > days) {
				matches.add(ue);
			}
		}

		bot.privmsg(bot.getAdminChannel(), bot.getString("adminQueryMatch", matches.size(), "lastseen=>" + days));
		if (matches.size() > 0) {
			StringBuilder sb = new StringBuilder();
			for (UserEntry ue : matches) {
				sb.append(ue.getNick() + " (Id: " + ue.getId() + "), ");
			}

			if (sb.length() > 200) {
				bot.privmsg(bot.getAdminChannel(), "Exceeded length limit, response sent via notice.");
				bot.notice(args[0].substring(1).split("!")[0], sb.toString().substring(0, sb.length() - 2));
			} else {
				bot.privmsg(bot.getAdminChannel(), sb.toString().substring(0, sb.length() - 2));
			}
		}
	}
}
