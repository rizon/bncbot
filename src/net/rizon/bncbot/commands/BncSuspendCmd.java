package net.rizon.bncbot.commands;

import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import net.rizon.bncbot.AbstractSimpleCommand;
import net.rizon.bncbot.UserDB.UserEntry;
import net.rizon.bncbot.UserDB.UserEntry.UserState;

public class BncSuspendCmd extends AbstractSimpleCommand {
	@Override
	public boolean prepare(String line) {
		super.prepare(line);

		// We want the message to look like this:
		// :nick!ident@host PRIVMSG #adminchan :command arg reason+++++
		// 0 1 2 3 4 5..6..7....
		return args[2].equalsIgnoreCase(bot.getAdminChannel()) && args.length >= 5;
	}

	@Override
	public void run() {
		String value = args[4];

		// Lookup user as nickname.
		UserEntry ue = bot.database.findUser(value);
		if (ue == null) {
			// Try as a integer.
			try {
				int intValue = Integer.parseInt(value);
				ue = bot.database.findUser(intValue);
			} catch (NumberFormatException e) {
				ue = null; // Oh well. :/
			}
		}

		// Check to make sure a user exists.
		if (ue == null) {
			bot.privmsg(bot.getAdminChannel(), bot.getString("adminNoMatch"));
			return;
		}
		
		// Connected check.
		if(!bot.getUserServerConnected(ue)) {
			bot.privmsg(bot.getAdminChannel(), bot.getString("errorBncNotConnected"));
			return;
		}

		if (args[3].equalsIgnoreCase(":.bncsuspend") || args[3].equalsIgnoreCase(":.bs")) {
			if (args.length < 6) {
				bot.privmsg(bot.getAdminChannel(), bot.getString("adminErrorReasonRequired"));
				return;
			}
			
			int time = 4320, reasonI = 5;
			try { 
				time = Integer.parseInt(args[5]);
				reasonI = 6;
			} catch (Exception e) {
			}
			
			String reason = argsv[reasonI];

			if (ue.getState() == UserState.Approved) {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd hh.mm");
				bot.database.suspendUser(ue, args[0].substring(1), reason, time);
				bot.bncManager.getUserServer(ue).blockUser(ue.getNick(), String.format("Temporary suspension %4$s min - [%2$s] %1$s (%3$s)", reason, args[0].substring(1).split("!")[0], sdf.format(new GregorianCalendar(TimeZone.getTimeZone("GMT")).getTime()), time));

				bot.privmsg(bot.getAdminChannel(), "The BNC has been suspended (" + time + " min expiry)");
			} else {
				bot.privmsg(bot.getAdminChannel(), bot.getString("genericErrorReason", bot.getString("adminErrorSuspensionMismatch")));
			}

		} else if (args[3].equalsIgnoreCase(":.bncunsuspend") || args[3].equalsIgnoreCase(":.bus")) {
			if (ue.getState() == UserState.Suspended) {
				bot.database.unsuspendUser(ue, args[0].substring(1));
				bot.bncManager.getUserServer(ue).unblockUser(ue.getNick());

				bot.privmsg(bot.getAdminChannel(), bot.getString("adminBncActionSilent", bot.getString("adminUnsuspended")));
			} else {
				bot.privmsg(bot.getAdminChannel(), bot.getString("genericErrorReason", bot.getString("adminErrorUnsuspensionMismatch")));
			}
		} else {
			// Bug.
		}
	}

}
