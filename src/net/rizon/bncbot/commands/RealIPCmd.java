package net.rizon.bncbot.commands;

import net.rizon.bncbot.AbstractSimpleCommand;
import net.rizon.bncbot.Helpers;
import net.rizon.bncbot.UserDB.UserEntry;

public class RealIPCmd extends AbstractSimpleCommand {
	@Override
	public boolean prepare(String line) {
		super.prepare(line);

		// Check for proper number of arguments.
		return args[2].equalsIgnoreCase(bot.config.getProperty("nick")) && args.length == 5;
	}

	@Override
	public void run() {
		// Do some stuff.
		String user = args[0].substring(1);
		String nick = user.split("!")[0];

		// Check to make sure is oper or me!
		//boolean ident = nick.equals("Nol888") || bot.lookupOper(nick);
		boolean ident = bot.lookupOper(nick);
		if (!ident) {
			// Silently ignore the command.
			bot.notice(nick, "Notauthed.");
			return;
		}

		// Find the main nick.
		/*String mainNick = bot.lookupNick(args[4]);
		if (mainNick == null) {
			bot.notice(nick, bot.getString("genericErrorReason", bot.getString("adminErrorNickResolution")));
			return;
		}*/

		// Get database entry.
		//UserEntry ue = bot.database.findUser(mainNick);
		UserEntry ue = bot.database.findUser(args[4]);
		if (ue == null) {
			bot.notice(nick, bot.getString("genericErrorReason", bot.getString("errorNoSuchUser") + " You must use the main nick, i.e. the one in the host: xxx.RizonBNC.yy.rizon.net"));
			return;
		} else {
			// Connected check.
			if(!bot.getUserServerConnected(ue)) {
				bot.privmsg(nick, bot.getString("errorBncNotConnected"));
				return;
			}
			
			try {
				String[] hosts = bot.bncManager.getUserServer(ue).getHosts(ue);
				bot.notice(nick, bot.getString("adminRealIP", ue.getNick(), hosts.length, hosts.length == 1 ? "" : "s", Helpers.implode(hosts, ", ")));
			} catch (RuntimeException re) {
				bot.notice(nick, bot.getString("genericErrorReason", bot.getString(re.getMessage())));
			}
		}
	}
}
