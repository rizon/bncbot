package net.rizon.bncbot;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.rizon.DatabaseConnection;
import net.rizon.bncmanager.BncServer;

public final class UserDB implements Iterable<UserDB.UserEntry> {
	private Map<Integer, UserEntry> entries;
	private Map<Integer, String> blacklist;
	private Map<String, Integer> nickToId;

	public UserDB() {
		this.rehash();
	}

	public int addUser(String nick, String hostmask, BncServer server, boolean override) {
		Connection conn = DatabaseConnection.getConnection();
		PreparedStatement ps = null;

		try {
			ps = conn.prepareStatement("INSERT INTO `bncbot_users` (`bnc_user_nick`,`bnc_user_hostmask`,`bnc_server_id`) VALUES (?,?,?);", Statement.RETURN_GENERATED_KEYS);

			ps.setString(1, nick);
			ps.setString(2, hostmask);
			ps.setInt(3, server.getId());
			ps.execute();

			ResultSet rs = ps.getGeneratedKeys();
			if (!rs.next()) {
				BncBot.get().errPrintln("Error adding user: MySQL did not return a AUTO_INCREMENT key.");
				return -1;
			}

			int newId = rs.getInt(1);

			synchronized (this) {
				this.entries.put(newId, new UserEntry(newId, nick, hostmask, server.getServerAbbr()));
			}
			this.nickToId.put(nick, newId);

			this.logEvent("A new BNC entry (id %s) was created for user %s!%s%s", newId, nick, hostmask, override == true ? " using override code." : "");

			return newId;
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		} finally {
			try {
				if (ps != null)
					ps.close();
			} catch (Exception e) {
			}
		}
	}

	public UserEntry findUser(String nick) {
		Integer id = this.nickToId.get(nick);
		return id != null ? findUser(id) : null;
	}

	public UserEntry findUser(int userID) {
		return this.entries.get(userID);
	}

	@Override
	public Iterator<UserEntry> iterator() {
		return this.entries.values().iterator();
	}

	public boolean approveUser(String nick, String approvalUser) {
		return approveUser(findUser(nick), approvalUser);
	}

	public boolean approveUser(int userID, String approvalUser) {
		return approveUser(findUser(userID), approvalUser);
	}

	public boolean approveUser(UserEntry ue, String aUser) {
		if (ue == null)
			return false;

		ue.approve(aUser);
		this.logEvent("BNC entry for user '%s' (id %s) was approved by %s", ue.getNick(), ue.getId(), aUser);
		this.saveData();

		return true;
	}

	public boolean rejectUser(String nick, String rejectionUser, String rejectionReason) {
		return rejectUser(findUser(nick), rejectionUser, rejectionReason);
	}

	public boolean rejectUser(int userID, String rejectionUser, String rejectionReason) {
		return rejectUser(findUser(userID), rejectionUser, rejectionReason);
	}

	public boolean rejectUser(UserEntry ue, String rUser, String rReason) {
		if (ue == null)
			return false;

		ue.reject(rUser, rReason);
		this.logEvent("BNC entry for user '%s' (id %s) was rejected/deleted by %s (reason: %s)", ue.getNick(), ue.getId(), rUser, rReason);
		this.saveData();

		return true;
	}

	public boolean suspendUser(String nick, String suspensionUser, String suspensionReason, int expiry) {
		return suspendUser(findUser(nick), suspensionUser, suspensionReason, expiry);
	}

	public boolean suspendUser(int userID, String suspensionUser, String suspensionReason, int expiry) {
		return suspendUser(findUser(userID), suspensionUser, suspensionReason, expiry);
	}

	public boolean suspendUser(UserEntry ue, String sUser, String sReason, int expiry) {
		if (ue == null)
			return false;

		ue.suspend(sUser, sReason);
		this.logEvent("BNC entry for user '%s' (id %s) was suspended by %s (reason: %s)", ue.getNick(), ue.getId(), sUser, sReason);
		this.saveData();

		try {
			Connection conn = DatabaseConnection.getConnection();
			PreparedStatement ps = conn.prepareStatement("INSERT INTO `bncbot_suspension` (`user_id`, `suspend_time`, `suspend_expiry`) VALUES (?,CURRENT_TIMESTAMP,?);");
			ps.setInt(1, ue.getId());
			ps.setTimestamp(2, new Timestamp(new Date().getTime() + expiry * 60000));
			ps.execute();
			ps.close();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	public boolean unsuspendUser(String nick, String suspensionUser) {
		return unsuspendUser(findUser(nick), suspensionUser);
	}

	public boolean unsuspendUser(int userID, String suspensionUser) {
		return unsuspendUser(findUser(userID), suspensionUser);
	}

	public boolean unsuspendUser(UserEntry ue, String sUser) {
		if (ue == null)
			return false;

		ue.approve(sUser);
		this.logEvent("BNC entry for user '%s' (id %s) was unsuspended by %s", ue.getNick(), ue.getId(), sUser);
		this.saveData();

		try {
			Connection conn = DatabaseConnection.getConnection();
			PreparedStatement ps = conn.prepareStatement("DELETE FROM `bncbot_suspension` WHERE `user_id` = ?;");
			ps.setInt(1, ue.getId());
			ps.execute();
			ps.close();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	public boolean moveUser(String nick, String moveUser, BncServer newServer) {
		return unsuspendUser(findUser(nick), moveUser);
	}

	public boolean moveUser(int userID, String moveUser, BncServer newServer) {
		return unsuspendUser(findUser(userID), moveUser);
	}

	public boolean moveUser(UserEntry ue, String moveUser, BncServer newServer) {
		if (ue == null)
			return false;

		ue.setNewBnc(moveUser, newServer);
		this.logEvent("BNC entry for user '%s' (id %s) was moved to '%s' by %s", ue.getNick(), ue.getId(), newServer.getServerAbbr(), moveUser);
		this.saveData();

		return true;
	}

	public boolean setNote(UserEntry ue, String notingUser, String note) {
		if (ue == null)
			return false;

		ue.setNote(notingUser, note);
		if (note != null)
			this.logEvent("BNC note for user '%s' (id %s) was added by %s: %s", ue.getNick(), ue.getId(), notingUser, note);
		else
			this.logEvent("BNC note for user '%s' (id %s) was deleted by %s", ue.getNick(), ue.getId(), notingUser);
		this.saveData();

		return true;
	}

	public boolean addBlacklist(String mask) throws IllegalArgumentException {
		if (!mask.contains("!") || !mask.contains("@"))
			throw new IllegalArgumentException("Invalid parameter: mask. Invalid hostmask.");

		Connection conn = DatabaseConnection.getConnection();
		PreparedStatement ps = null;

		try {
			ps = conn.prepareStatement("INSERT INTO `bncbot_bl` (`bl_data`) VALUES (?);", Statement.RETURN_GENERATED_KEYS);

			ps.setString(1, mask);
			ps.execute();

			ResultSet rs = ps.getGeneratedKeys();
			if (!rs.next()) {
				BncBot.get().errPrintln("Error adding blacklist entry: MySQL did not return a AUTO_INCREMENT key.");
				return false;
			}

			int newId = rs.getInt(1);
			synchronized (this.blacklist) {
				this.blacklist.put(newId, mask);
			}

			this.logEvent("Blacklist entry (id %s) added to database: %s", newId, mask);

			return true;
		} catch (SQLException e) {
			throw new IllegalArgumentException("Invalid parameter: mask. Hostmask already exists in database.");
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				if (ps != null)
					ps.close();
			} catch (Exception e) {
			}
		}
	}

	public boolean remBlacklist(int id) {
		if (!this.blacklist.containsKey(id))
			return false;

		Connection conn = DatabaseConnection.getConnection();

		try {
			PreparedStatement ps = conn.prepareStatement("DELETE FROM `bncbot_bl` WHERE `bl_id` = ? LIMIT 1;");

			ps.setInt(1, id);
			ps.execute();

			synchronized (this.blacklist) {
				this.blacklist.remove(id);
			}
			this.logEvent("Blacklist entry (id %s) deleted.", id);

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public int isBlacklisted(String mask) throws IllegalArgumentException {
		if (!mask.contains("!") || !mask.contains("@"))
			throw new IllegalArgumentException("Invalid parameter: mask. Invalid hostmask.");

		// This is expensive, find a better way maybe.
		synchronized (this.blacklist) {
			for (Integer key : this.blacklist.keySet())
				if (Helpers.matchesWithWildcards(mask, this.blacklist.get(key)))
					return key;
		}

		return -1;
	}

	public Map<Integer, String> getBlacklist() {
		return Collections.unmodifiableMap(this.blacklist);
	}

	public List<UserLogEntry> getClientLog(UserEntry ue) {
		List<UserLogEntry> ule = new ArrayList<UserLogEntry>();

		try {
			Connection conn = DatabaseConnection.getConnection();
			PreparedStatement ps = conn.prepareStatement("SELECT event_time, INET_NTOA(ip) AS client_ip, event_type, was_last_client FROM bncbot_conn_log WHERE user_id = ? ORDER BY event_time DESC");
			ps.setInt(1, ue.getId());

			ResultSet rs = ps.executeQuery();

			while (rs.next())
				ule.add(new UserLogEntry(rs.getString("event_type"), new Date(rs.getTimestamp("event_time").getTime()), rs.getString("client_ip"), rs.getBoolean("was_last_client")));

			ps.close();
			return ule;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public UserLogEntry getLastClientEvent(UserEntry ue) {
		try {
			Connection conn = DatabaseConnection.getConnection();
			PreparedStatement ps = conn.prepareStatement("SELECT event_time, INET_NTOA(ip) AS client_ip, event_type, was_last_client FROM bncbot_conn_log WHERE user_id = ? ORDER BY event_time DESC LIMIT 1");
			ps.setInt(1, ue.getId());

			ResultSet rs = ps.executeQuery();
			UserLogEntry entry = null;

			if (rs.first())
				entry = new UserLogEntry(rs.getString("event_type"), new Date(rs.getTimestamp("event_time").getTime()), rs.getString("client_ip"), rs.getBoolean("was_last_client"));

			ps.close();
			return entry;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public Map<Integer, UserLogEntry> getApprovedUserActivity() {
		try {
			Connection conn = DatabaseConnection.getConnection();
			PreparedStatement ps = conn.prepareStatement("SELECT a.bnc_user_id, b.event_time, b.event_type, INET_NTOA(b.ip) AS client_ip, b.was_last_client FROM `bncbot_users` a LEFT JOIN bnc_lastseen b ON a.bnc_user_id = b.user_id WHERE a.bnc_state = 1");

			ResultSet rs = ps.executeQuery();
			Map<Integer, UserLogEntry> result = new HashMap<Integer, UserLogEntry>();

			while (rs.next()) {
				String event_type = rs.getString("event_type");

				if (rs.wasNull()) {
					result.put(rs.getInt("bnc_user_id"), null);
				} else {
					result.put(rs.getInt("bnc_user_id"), new UserLogEntry(event_type, new Date(rs.getTimestamp("event_time").getTime()), rs.getString("client_ip"), rs.getBoolean("was_last_client")));
				}
			}

			ps.close();
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public String heuristicSimilarity(String mainNick, int victim) {
		try {
			ArrayList<String> sims = new ArrayList<String>();

			synchronized (this.entries) {
				for (UserEntry s : this.entries.values())
					if (editDistance(mainNick, s.getNick()) < 2 && s.getId() != victim)
						sims.add(s.getNick());
			}

			if (sims.size() == 0)
				return null;

			String[] a = new String[sims.size()];
			sims.toArray(a);
			return "Nick Similarity: The main nick is similar to the following: " + Helpers.implode(a, ", ");
		} catch (Throwable t) {
			return "";
		}
	}

	public String heuristicHostmask(String host, int victim) {
		try {
			host = host.toLowerCase();
			ArrayList<String> sims = new ArrayList<String>();

			synchronized (this.entries) {
				for (UserEntry s : this.entries.values()) {
					String[] a = s.getHostmask().toLowerCase().split("@");
					String h = a.length > 1 ? a[1] : "";
					if (h.equals(host) && s.getId() != victim)
						sims.add(s.getId() + "");
				}
			}

			if (sims.size() == 0)
				return null;

			String[] a = new String[sims.size()];
			sims.toArray(a);
			return "Hostmask check: The following entries share a host with the request: " + Helpers.implode(a, ", ");
		} catch (Throwable t) {
			return "";
		}
	}

	public String heuristicIdent(String ident, int victim) {
		try {
			ident = ident.toLowerCase();
			if (ident.startsWith("~"))
				ident = ident.substring(1);
			if (ident.length() <= 2)
				return null; // Ignore - too short.
			ArrayList<String> sims = new ArrayList<String>();

			synchronized (this.entries) {
				for (UserEntry s : this.entries.values()) {
					if (s.getNick().toLowerCase().startsWith(ident) && s.getId() != victim)
						sims.add(s.getNick());
				}
			}

			if (sims.size() == 0)
				return null;

			String[] a = new String[sims.size()];
			sims.toArray(a);
			return "Ident/username check: The user's ident is the prefix for the following users: " + Helpers.implode(a, ", ");
		} catch (Throwable t) {
			return "";
		}
	}

	public ArrayList<String> applyHeuristics(String mainNick, String mask, int victim) {
		ArrayList<String> warnings = new ArrayList<String>();
		String ident = mask.split("@")[0];
		String host = mask.split("@")[1];

		warnings.add(heuristicSimilarity(mainNick, victim));
		warnings.add(heuristicIdent(ident, victim));
		warnings.add(heuristicHostmask(host, victim));

		return warnings;
	}
	public ArrayList<String> applyHeuristics(String mainNick, String mask) {
		return applyHeuristics(mainNick, mask, 0);
	}

	public void saveData(boolean now) {
		String query = "REPLACE INTO `bncbot_users` VALUES (?,?,?,?,?,?,?,?,?,?,?);";
		String blQuery = "REPLACE INTO `bncbot_bl` VALUES (?,?);";
		String bncLookupQuery = "SELECT `server_id`, `server_abbr` FROM `bncbot_bncs` ORDER BY `server_id` ASC;";
		long saveStart = System.currentTimeMillis();

		BncBot.get().stdPrintln("User database saving...");

		Connection conn = DatabaseConnection.getConnection();
		int changed = 0;
		Hashtable<String, Integer> bncLookupTable = new Hashtable<String, Integer>();
		try {
			PreparedStatement ps = conn.prepareStatement(query);
			PreparedStatement psbl = conn.prepareStatement(blQuery);

			PreparedStatement pslookup = conn.prepareStatement(bncLookupQuery);
			ResultSet rs = pslookup.executeQuery();
			while (rs.next()) {
				bncLookupTable.put(rs.getString("server_abbr").toLowerCase(), rs.getInt("server_id"));
			}
			rs.close();
			pslookup.close();

			synchronized (this.entries) {
				for (UserEntry ue : this.entries.values()) {
					if (ue.isModified() || now) {
						ps.setInt(1, ue.getId());
						ps.setString(2, ue.getNick());
						ps.setString(3, ue.getHostmask());
						ps.setTimestamp(4, new Timestamp(ue.getTimeRequested().getTime()));
						ps.setInt(5, ue.getState().getAsInt());
						ps.setString(6, ue.getActionByNick());
						ps.setString(7, ue.getActionByHostmask());
						ps.setTimestamp(8, (ue.getState() != UserEntry.UserState.NotProcessed) ? new Timestamp(ue.getActionTime().getTime()) : null);
						ps.setString(9, ue.getActionReason());
						ps.setString(10, ue.getNote());
						ps.setInt(11, bncLookupTable.get(ue.getBncServerAbbr().toLowerCase()));

						ps.addBatch();

						ue.setNotModified();
						changed++;
					}
				}
			}

			ps.executeBatch();

			synchronized (this.blacklist) {
				for (Integer key : this.blacklist.keySet()) {
					psbl.setInt(1, key);
					psbl.setString(2, this.blacklist.get(key));

					psbl.addBatch();
				}
			}

			psbl.executeBatch();

			psbl.close();
			ps.close();
			BncBot.get().stdPrintln("User database flushed %s entries (%s blacklist entries) to MySQL in %s ms.", changed, this.blacklist.size(), System.currentTimeMillis() - saveStart);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void saveData() {
		saveData(false);
	}

	public void rehash() {
		Connection conn = DatabaseConnection.getConnection();
		entries = new HashMap<Integer, UserEntry>();
		blacklist = new HashMap<Integer, String>();
		nickToId = new HashMap<String, Integer>();
		BncBot.get().stdPrintln("User database starting load from MySQL.");

		// Try to load from the DB.
		try {
			PreparedStatement ps = conn.prepareStatement("SELECT `u`.*, `b`.`server_abbr` FROM `bncbot_users` as u LEFT JOIN `bncbot_bncs` AS b ON `b`.`server_id` = `u`.`bnc_server_id`;");
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				entries.put(rs.getInt("bnc_user_id"), UserEntry.fromResultSet(rs));
				nickToId.put(rs.getString("bnc_user_nick"), rs.getInt("bnc_user_id"));
			}

			rs.close();
			ps.close();

			// ====================

			ps = conn.prepareStatement("SELECT * FROM `bncbot_bl`;");
			rs = ps.executeQuery();

			while (rs.next()) {
				blacklist.put(rs.getInt("bl_id"), rs.getString("bl_data"));
			}

			rs.close();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		BncBot.get().stdPrintln("User database loaded %s entries (%s blacklist entries) from MySQL.", this.entries.size(), this.blacklist.size());
	}

	public void logConnectEvent(String username, String type, String ip, boolean lastClient) {
		Connection conn = DatabaseConnection.getConnection();
		PreparedStatement ps = null;
		UserEntry ue = findUser(username);

		if (ue == null) {
			System.out.println("log connect event for unknown user " + username);
			return;
		}

		try {
			ps = conn.prepareStatement("INSERT INTO `bncbot_conn_log` (user_id, ip, event_type, was_last_client) VALUES (?, INET_ATON(?), ?, ?);");

			ps.setInt(1, ue.getId());
			ps.setString(2, ip);
			ps.setString(3, type);
			ps.setBoolean(4, lastClient);

			ps.execute();
			ps.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private boolean logEvent(String event) {
		Connection conn = DatabaseConnection.getConnection();
		PreparedStatement ps = null;

		try {
			ps = conn.prepareStatement("INSERT INTO `bncbot_log` VALUES (NULL, ?, NULL);");

			ps.setString(1, event);
			ps.execute();

			return ps.getUpdateCount() == 1;
		} catch (Exception e) {
			e.printStackTrace();

			return false;
		} finally {
			try {
				if (ps != null)
					ps.close();
			} catch (Exception e) {
			}
		}
	}

	private boolean logEvent(String eventFormat, Object... args) {
		return logEvent(String.format(eventFormat, args));
	}

	private static int minimum(int a, int b, int c) {
		return Math.min(Math.min(a, b), c);
	}

	private static int editDistance(CharSequence str1, CharSequence str2) {
		int[][] distance = new int[str1.length() + 1][str2.length() + 1];

		for (int i = 0; i <= str1.length(); i++)
			distance[i][0] = i;
		for (int j = 0; j <= str2.length(); j++)
			distance[0][j] = j;

		for (int i = 1; i <= str1.length(); i++)
			for (int j = 1; j <= str2.length(); j++)
				distance[i][j] = minimum(distance[i - 1][j] + 1, distance[i][j - 1] + 1, distance[i - 1][j - 1] + ((str1.charAt(i - 1) == str2.charAt(j - 1)) ? 0 : 1));

		return distance[str1.length()][str2.length()];
	}

	public static class UserEntry {
		private int id;
		private String nick;
		private String hostmask;
		private Date timeRequested;

		private UserState state;
		private String actionBy;
		private String actionByHostmask;
		private Date actionTime;

		private String actionReason;
		private String note;

		private String bncServerAbbr;

		// Flag to tell if the entry is modified
		// and must be flushed to DB.
		private boolean modified;

		public UserEntry(int id, String nick, String hostmask, Date timeRequested, String serverAbbr) {
			this.id = id;
			this.nick = nick;
			this.hostmask = hostmask;
			this.timeRequested = timeRequested;

			this.state = UserState.NotProcessed;
			this.actionBy = null;
			this.actionByHostmask = null;
			this.actionTime = null;

			this.actionReason = null;
			this.note = null;

			this.bncServerAbbr = serverAbbr;

			this.modified = true;
		}

		public UserEntry(int id, String nick, String hostmask, String serverAbbr) {
			this(id, nick, hostmask, new Date(), serverAbbr);
		}

		public int getId() {
			return this.id;
		}

		public String getNick() {
			return this.nick;
		}

		public String getHostmask() {
			return this.hostmask;
		}

		public Date getTimeRequested() {
			return this.timeRequested;
		}

		public UserState getState() {
			return this.state;
		}

		public String getActionByNick() {
			return (this.state != UserState.NotProcessed) ? this.actionBy : null;
		}

		public String getActionByHostmask() {
			return (this.state != UserState.NotProcessed) ? this.actionByHostmask : null;
		}

		public Date getActionTime() {
			return (this.state != UserState.NotProcessed) ? this.actionTime : null;
		}

		public String getActionReason() {
			return (this.state != UserState.NotProcessed && this.state != UserState.Approved) ? this.actionReason : null;
		}

		public String getNote() {
			return this.note;
		}

		public String getBncServerAbbr() {
			return this.bncServerAbbr;
		}

		@Override
		public String toString() {
			return String.format("[%s] %s (%s)", this.id, this.nick, this.bncServerAbbr) + (modified ? " (dirty)" : "");
		}

		private void approve(String user) {
			this.state = UserState.Approved;
			this.actionBy = user.split("!")[0];
			this.actionByHostmask = user.split("!")[1];
			this.actionTime = new Date();

			this.modified = true;
		}

		private void reject(String user, String reason) {
			this.state = UserState.Rejected;
			this.actionBy = user.split("!")[0];
			this.actionByHostmask = user.split("!")[1];
			this.actionTime = new Date();
			this.actionReason = reason;

			this.modified = true;
		}

		private void suspend(String user, String reason) {
			this.state = UserState.Suspended;
			this.actionBy = user.split("!")[0];
			this.actionByHostmask = user.split("!")[1];
			this.actionTime = new Date();
			this.actionReason = reason;

			this.modified = true;
		}

		private void setNewBnc(String user, BncServer server) {
			this.bncServerAbbr = server.getServerAbbr();
			this.actionBy = user.split("!")[0];
			this.actionByHostmask = user.split("!")[1];
			this.actionTime = new Date();

			this.modified = true;
		}

		private void setNote(String user, String note) {
			if (note != null)
				note = user.split("!")[0] + ": " + note;
			this.note = note;

			this.modified = true;
		}

		private boolean isModified() {
			return this.modified;
		}

		private void setNotModified() {
			this.modified = false;
		}

		public static UserEntry fromResultSet(ResultSet rs) {
			try {
				UserEntry ue = new UserEntry(rs.getInt("bnc_user_id"), rs.getString("bnc_user_nick"), rs.getString("bnc_user_hostmask"), new Date(rs.getTimestamp("bnc_time_requested").getTime()), rs.getString("server_abbr"));

				ue.state = UserState.valueOf(rs.getInt("bnc_state"));
				ue.actionBy = rs.getString("bnc_action_by");
				ue.actionByHostmask = rs.getString("bnc_action_by_hostmask");
				ue.actionTime = (ue.getState() != UserState.NotProcessed) ? new Date(rs.getTimestamp("bnc_action_time").getTime()) : null;

				ue.actionReason = rs.getString("bnc_action_reason");
				ue.note = rs.getString("bnc_note");

				ue.modified = false;

				return ue;
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}

		public enum UserState {
			NotProcessed {
				@Override
				public int getAsInt() {
					return 0;
				}
			},
			Approved {
				@Override
				public int getAsInt() {
					return 1;
				}
			},
			Rejected {
				@Override
				public int getAsInt() {
					return 2;
				}
			},
			Suspended {
				@Override
				public int getAsInt() {
					return 3;
				}
			};

			public abstract int getAsInt();

			public static UserState valueOf(int value) {
				for (UserState as : UserState.values()) {
					if (as.getAsInt() == value)
						return as;
				}
				throw new AssertionError("Invalid approval state.");
			}
		}
	}

	public static class UserLogEntry {
		private String type;
		private Date date;
		private String ip;
		private boolean lastClient;

		private UserLogEntry(String type, Date date, String ip, boolean lastClient) {
			this.type = type;
			this.date = date;
			this.ip = ip;
			this.lastClient = lastClient;
		}

		public String getEntryType() {
			return type;
		}

		public Date getEntryTime() {
			return date;
		}

		public String getClientIp() {
			return ip;
		}

		public boolean wasLastClient() {
			return lastClient;
		}
	}

}
