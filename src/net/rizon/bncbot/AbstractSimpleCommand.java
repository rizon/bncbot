package net.rizon.bncbot;


public abstract class AbstractSimpleCommand implements Command {
	protected String line, args[], argsv[];
	protected BncBot bot = BncBot.get();

	@Override
	public boolean prepare(String line) {
		// Store the line.
		this.line = line;

		// Split to space-delimited words.
		this.args = line.split("\\s+");

		// argsv[0] would have args[0->(length-1)]
		// argsv[1] would have args[1->(length-1)], etc.
		this.argsv = new String[this.args.length];

		for(int i = 0; i < this.args.length; i++) {
			StringBuilder sb = new StringBuilder();
			for(int j = i; j < this.args.length; j++) {
				sb.append(args[j]);
				if(j + 1 < this.args.length) sb.append(" ");
			}
			this.argsv[i] = sb.toString().trim();
		}

		return false;
	}
}
