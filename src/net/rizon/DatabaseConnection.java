package net.rizon;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.TimeZone;

import net.rizon.bncbot.BncBot;

public final class DatabaseConnection {
	private static Connection con = null;

	private static String[] config = null;
	private static int[] password = null;

	static {
		Runtime.getRuntime().addShutdownHook(new ShutdownHandler());
	}

	public static Connection getConnection() {
		if (!isInitialized())
			throw new RuntimeException("DatabaseConnection not initialized");

		try {
			if ((con != null) && con.isValid(2)) {
				return con;
			} else { // Attempt reconnect.
				if (con != null)
					con.close();

				DatabaseConnection.connect();

				if (con == null) {
					throw new RuntimeException("MySQL server has gone away. Attempted reconnection failed.");
				}

				return con;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (AbstractMethodError e) {
			DatabaseConnection.connect();

			if (con == null) {
				throw new RuntimeException("MySQL server has gone away. Attempted reconnection failed.");
			}

			return con;
		}
		return con;
	}

	public static boolean isInitialized() {
		return config != null && password != null;
	}

	public static void setProps(Properties aProps) {
		config = new String[] { aProps.getProperty("sqlDriver"), aProps.getProperty("sqlUrl"), aProps.getProperty("sqlUser") };

		String pw = aProps.getProperty("sqlPass");
		password = new int[pw.length()];
		for (int i = 0, k = 10; i < pw.length(); i++, k = k > 80 ? 10 : k + 1)
			password[i] = pw.charAt(i) ^ k;
	}

	private static void connect() {
		if (BncBot.get() != null)
			BncBot.get().stdPrintln("DatabaseConnection initiating connection to MySQL...");

		String pwd = "";
		for (int i = 0, k = 10; i < password.length; i++, k = k > 80 ? 10 : k + 1)
			pwd += Character.toString((char) (password[i] ^ k));

		try {
			Class.forName(config[0]);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		try {
			con = DriverManager.getConnection(config[1], config[2], pwd);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException("Error connecting to MySQL: " + e.getMessage());
		}

		try {
			TimeZone tz = TimeZone.getDefault();
			Statement s = con.createStatement();
			s.execute("SET time_zone = \"" + tz.getID() + "\"");
			s.close();
		} catch (Exception e) {
			BncBot.get().errPrintln("Failed to set MySQL connection time zone.");
			e.printStackTrace();
		}

		if (BncBot.get() != null)
			BncBot.get().stdPrintln("MySQL connection established.");
	}

	private static class ShutdownHandler extends Thread {
		@Override
		public void run() {
			try {
				DatabaseConnection.con.close();
				DatabaseConnection.con = null;
			} catch (SQLException e) {
			} catch (NullPointerException npe) {
			}
		}
	}
}
